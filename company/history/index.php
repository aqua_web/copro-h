<?php
//必須ファイル読み込み
require_once('../../system/function.php');
//ページ設定
$str_dsc =  $SITE_NAME . 'の会社沿革をご紹介します。';
$str_tit = '会社沿革';
$str_kwd = '会社沿革';

//関連リンク設定
$rel_array = array(
	'/company/message/' => '代表挨拶',
	'/company/philosophy/' => '企業理念',
	'/company/group/' => 'コプログループについて',
	'/company/profile/' => '会社概要'
);

?>
<!DOCTYPE html>
<html lang="ja">
<head>
	<?php include( $DOC_ROOT . '/inc/meta_inc.php');?>
	<link rel="stylesheet" type="text/css" href="/assets/css/company.css">
</head>
<body id="<?php echo $str_page_id; ?>">
	<div id="allWrap">
		<?php include( $DOC_ROOT . '/inc/header_inc.php');?>
			<div class="contWrap">
				<div class="cont980 ibWrap">
					<nav class="breadCrumb">
						<ul>
							<li><a href="/">ホーム</a></li>
							<li><a href="/company/"><i class="fa fa-chevron-right"></i>企業情報</a></li>
							<li><i class="fa fa-chevron-right"></i><?php echo $str_tit; ?></li>
						</ul>
					</nav>
					<h2 class="h2_line h2_short"><?php echo $str_tit; ?></h2>
					<div id="mainWrap" class="contMain">
						<main>
							<article>
								<!-- content -->
								<dl class="dl_history">
									<dt>2006&nbsp;年<span>10月</span></dt><dd><a href="http://www.copro-e.co.jp/" target="_blank">株式会社コプロ・エンジニアード</a>　設立<span>（名古屋市中区）</span></dd>
								</dl>
								<!--dl class="dl_history">
									<dt>2007&nbsp;年<span>2月</span></dt><dd>株式会社コプロ・エンジニアード　横浜支店　開設<span>（横浜市西区）</span></dd>
								</dl>
								<dl class="dl_history">
									<dt>2008&nbsp;年<span>3月</span></dt><dd>株式会社コプロ・エンジニアード　本社・名古屋支店　移転<span>（名古屋市中区）</span></dd>
									<dt><span>8月</span></dt><dd>株式会社コプロ・エンジニアード　東京支店　開設<span>（東京都新宿区）</span></dd>
								</dl-->
								<!--dl class="dl_history">
									<dt>2009&nbsp;年<span>9月</span></dt><dd>GIP株式会社設立<span>（名古屋市中区）</span></dd>
									<dt>2009&nbsp;年<span>10月</span></dt><dd>株式会社コプロ・エンジニアード　札幌支店　開設<span>（札幌市中央区）</span></dd>
								</dl>
								<dl class="dl_history">
									<dt>2011&nbsp;年<span>3月</span></dt><dd>株式会社コプロ・エンジニアード　大阪支店　開設<span>（大阪市北区）</span></dd>
									<dt><span>9月</span></dt><dd>株式会社コプロ・エンジニアード　首都圏支店　開設<span>（東京都中央区）</span></dd>
								</dl>
								<dl class="dl_history">
									<dt>2012&nbsp;年<span>3月</span></dt><dd>株式会社コプロ・エンジニアード　プライバシーマーク　<span>認定17001063（01）</span></dd>
									<dt><span>4月</span></dt><dd>株式会社コプロ・エンジニアード　横浜支店　移転<span>（横浜市神奈川区）</span> </dd>
									<dt><span>7月</span></dt><dd>株式会社コプロ・エンジニアード　東京支店　移転<span>（東京都新宿区）</span></dd>
									<dt><span>10月</span></dt><dd>株式会社コプロ・エンジニアード　福岡支店　開設<span>（福岡市博多区）</span></dd>
								</dl-->
								<dl class="dl_history">
									<dt>2013&nbsp;年<span>12月</span></dt><dd>オリジナルキャラクター発表「コプ郎」誕生<!--br><span><a href="http://www.copro-e.co.jp/news/140106.pdf" target="_blank">第1回無担保社債（私募債）発行</a></span--></dd>
									<!--dt>2013&nbsp;年</dt><dd></dd><span>4月</span></dt><dd>株式会社コプロ・エンジニアード　仙台支店　開設<span>（仙台市青葉区）</span></dd-->
								</dl>
								<!--dl class="dl_history">
									<dt>2014&nbsp;年<span>2月</span></dt><dd><a href="http://www.copro-e.co.jp/news/140210.pdf" target="_blank">第2回無担保社債（私募債）発行 </a></dd>
									<dt><span>3月</span></dt><dd>株式会社コプロ・エンジニアード　プライバシーマーク　認定17001063<span>（02）</span></dd>
									<dt><span>4月</span></dt><dd>株式会社コプロ・エンジニアード　静岡サテライトオフィス　開設<span>（静岡市葵区）</span> </dd>
									<dt><span>6月</span></dt><dd>株式会社コプロ・エンジニアード　広島支店　開設<span>（広島市南区）</span></dd>
									<dt><span>9月</span></dt><dd>株式会社コプロ・エンジニアード　金沢サテライトオフィス　開設<span>（金沢市駅西本町）</span> </dd>
									<dt><span>10月</span></dt><dd>株式会社コプロ・エンジニアード　千葉サテライトオフィス　開設<span>（千葉市中央区）</span><br>株式会社コプロ・エンジニアード　姫路サテライトオフィス　開設<span>（姫路市豊沢町）</span></dd>
									<dt><span>11月</span></dt><dd>株式会社コプロ・エンジニアード　アカデミア事業部　開設<span>（名古屋市中区）</span></dd>
								</dl-->
								<dl class="dl_history">
									<dt>2015&nbsp;年<span>5月</span></dt><dd>ホールディングス<span>（持株会社）</span>体制に移行 </dd>
									<!--dt>2015&nbsp;年<span>3月</span></dt><dd>株式会社コプロ・エンジニアード　熊本サテライトオフィス　開設<span>（熊本市中央区）</span><br><a href="https://www.copro-e.co.jp/news/150331.pdf" target="_blank">第3回無担保社債（私募債）発行</a></dd>
									<dt><span>5月</span></dt><dd>ホールディングス<span>（持株会社）</span>体制に移行 </dd>
									<dt><span>7月</span></dt><dd>株式会社コプロ・エンジニアード　関東支店　開設<span>（東京都中央区）</span> </dd>
									<dt><span>12月</span></dt><dd>株式会社コプロ・エンジニアード　高松サテライトオフィス　開設<span>（高松市寿町）</span></dd-->
								</dl>
                                <!--2016-->
								<dl class="dl_history">
									<dt>2016&nbsp;年<span>3月</span></dt><dd><a href="https://www.copro-e.co.jp/news/160331.pdf" target="_blank">無担保変動利付社債（私募債）発行</a>
                                    <br>LEADER'S AWARD 2016にノミネートされました
                                    </dd>
									<!--dt>2016&nbsp;年<span>3月</span></dt><dd>株式会社コプロ・エンジニアード　<span>プライバシーマーク　認定17001063（03）</span><br><a href="https://www.copro-e.co.jp/news/160331.pdf" target="_blank">第4回無担保変動利付社債（私募債）発行</a></dd>
									<dt><span>4月</span></dt><dd>株式会社コプロ・エンジニアード　MC事業部　開設<span>（関東支店から改称、東京都中央区）</span></dd-->
									<dt><span>5月</span></dt><dd>株式会社コプロ・ホールディングス　移転<span>（名古屋市中村区）</span>
                                    <!--br>株式会社コプロ・エンジニアード　本社・名古屋支店　移転<span>（名古屋市中村区）</span></span-->
<!--

									<br>株式会社コプロ・ソリューションズ　移転<span>（名古屋市中村区）</span>
-->
									</dd>

									<dt><span>7月</span></dt><dd>株式会社コプロ・ホールディングス　10周年記念書籍『NUMBER.C』を発刊</dd>
									<dt><span>9月</span></dt><dd><a href="http://www.copro-e.co.jp/news/160926.pdf" target="_blank">無担保社債（私募債）発行</a><br><a href="http://www.copro-e.co.jp/news/160930.pdf" target="_blank">無担保社債（SMBCなでしこ私募債）発行</a></dd>
									<!--dt><span>11月</span></dt><dd>株式会社コプロ・エンジニア―ド　MC事業部を「MC支店」に名称変更</dd-->
									<dt><span>10月</span></dt><dd>株式会社コプロ・ホールディングス　公式サイト新規OPEN<br>
									<a href="http://www.smbc.co.jp/news/j601269_01.html" target="_blank">株式会社三井住友銀行様のウェブサイトにて、当社に関する内容を掲載頂きました</a>
                                    </dd>
									<dt><span>12月</span></dt><dd><a href="http://www.copro-h.co.jp/publicity/">「ブラマヨの質問ズケ！（MBS）」に代表清川が出演致しました</a><br>
									<a href="https://www.copro-h.co.jp/holdings/91/">「女性活躍のグロース企業」認定により、日本経済新聞にロゴ掲載されました</a></dd>
								</dl>

								<!--dl class="dl_history">
									<dt>2017&nbsp;年<span>3月</span></dt><dd>株式会社コプロ・エンジニアード　仙台支店　移転（仙台市青葉区）</dd>
									<dt><span>4月</span></dt><dd>株式会社コプロ・エンジニアード　大宮支店　開設（さいたま市大宮区）<br>
																			株式会社コプロ・エンジニアード　東京支店から東京第一支店へ改称<br>
																			株式会社コプロ・エンジニアード　首都圏支店から東京第二支店へ改称<br>
																			株式会社コプロ・エンジニアード　<a href="/engineered/128/">MC支店廃止</a><br>
									株式会社コプロ・エンジニアード　東京第一支店　移転（東京都中央区）<br>
									株式会社コプロ・エンジニアード　東京第二支店　移転（東京都中央区）</dd>
									<dt><span>6月</span></dt><dd>株式会社コプロ・エンジニアード　札幌支店　移転（北海道札幌市）</dd>
									<dt><span>7月</span></dt><dd>株式会社コプロ・エンジニアード　名古屋支店　移転（名古屋市中村区）</dd>
									<dt><span>8月</span></dt><dd>株式会社コプロ・エンジニアード　アカデミアセンターから<a href="http://www.copro-e.co.jp/news/btc.html" target="_blank">監督のタネ</a>へ改称</dd>
									<dt><span>9月</span></dt><dd>株式会社コプロ・エンジニアード　岡山サテライトオフィス　開設　（岡山市北区）<br>株式会社コプロ・エンジニアード　郡山サテライトオフィス　開設　（郡山市駅前）<br>株式会社コプロ・エンジニアード　四日市サテライトオフィス　開設　（四日市市諏訪町）</dd>
									<dt><span>10月</span></dt><dd>株式会社コプロ・エンジニアード　神戸支店　開設　(大阪市北区)</dd>
								</dl-->
								<!--2017-->
								<dl class="dl_history">
									<dt>2017&nbsp;年<span>3月</span></dt><dd>LEADER'S AWARD 2017にノミネートされました</dd>
									<dt><span>9月</span></dt><dd><a href="https://www.copro-h.co.jp/holdings/247/">働き方改革を実践した企業として、最優秀賞を受賞致しました</a></dd>
									<dt><span>10月</span></dt><dd><a href="https://www.copro-h.co.jp/holdings/249/">代表清川がEYアントレプレナー賞を受賞致しました</a><br>
									<a href="https://www.copro-h.co.jp/holdings/280/">「働き方改革実現アワード」受賞に関する記事が「月刊BOSS」に掲載されました</a><br>
                                    <a href="https://ashitanojinji.jp/event-report/2329" target="_blank">人事評価制度導入事例として「あしたの人事online」に掲載されました</a><br>
                                    <a href="http://diamond.jp/articles/-/145031" target="_blank">当社の評価制度に関する記事が「ダイヤモンドONLINE」に掲載されました</a>
                                    </dd>
									<dt><span>12月</span></dt>
                                    <dd>人事評価制度導入事例として「月刊人事マネジメント」に記事掲載されました</dd>
								</dl>
								<!--2018-->
								<dl class="dl_history">
									<dt>2018&nbsp;年<span>3月</span></dt>
                                    <dd>人事評価制度導入事例として「週刊ダイヤモンド」に記事掲載されました
									<br>週刊エコノミストに記事掲載されました
                                    </dd>
								</dl>
								<!-- content end -->
								<?php echo makeRelational($rel_array) ?>
							</article>
						</main>
					</div><!-- mainWrap end -->
					<?php include( $DOC_ROOT . '/inc/side_inc.php');?>
				</div><!-- cont980 end -->
				<?php include( $DOC_ROOT . '/inc/footer_inc.php');?>
			</div><!-- contWrap end -->
		<?php include( $DOC_ROOT . '/inc/script_inc.php');?>
	</div><!-- allWrap end -->
</body>
</html>
