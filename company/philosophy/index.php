<?php
//必須ファイル読み込み
require_once('../../system/function.php');
//ページ設定
$str_h1 = '企業理念';
$str_dsc = $SITE_NAME . 'の企業理念をご紹介します。';
$str_tit = '企業理念';
$str_kwd = '企業理念';

//関連リンク設定
$rel_array = array(
	'/company/message/' => '代表挨拶',
	'/company/group/' => 'コプログループについて',
	'/company/history/' => '会社沿革',
	'/company/profile/' => '会社概要'
);
?>
<!DOCTYPE html>
<html lang="ja">
<head>
	<?php include( $DOC_ROOT . '/inc/meta_inc.php');?>
	<link rel="stylesheet" type="text/css" href="/assets/css/company.css">
</head>
<body id="<?php echo $str_page_id; ?>">
	<div id="allWrap">
		<?php include( $DOC_ROOT . '/inc/header_inc.php');?>
			<div class="contWrap">
				<div class="cont980 ibWrap">
					<nav class="breadCrumb">
						<ul>
							<li><a href="/">ホーム</a></li>
							<li><a href="/company/"><i class="fa fa-chevron-right"></i>企業情報</a></li>
							<li><i class="fa fa-chevron-right"></i><?php echo $str_tit; ?></li>
						</ul>
					</nav>
					<h2 class="h2_line h2_short"><?php echo $str_tit; ?></h2>
					<div id="mainWrap" class="contMain">
						<main>
							<article>
								<!-- content -->
								<div class="div_philosophyWrap">
									<h3>コプロの想い</h3>
									<p>
										人を愛し、愛される会社
									</p>
									<h3>COPRO Mission (存在意義)</h3>
									<p>
										人と人をつなぎ<br>
										人に満足と感動を与え<br>
										人と成長し続け<br>
										日本を元気にする
									</p>
									<h3 class="h3_spirit">COPRO Spirit</h3>
									<dl class="dl_philosophy">
										<dt><span>C</span>ustomer satisfaction</dt>
										<dd>「強烈な顧客志向」を持ち、「満足」を超えた「感動」に値するサービスを提供し続ける。</dd>
										<dt><span>O</span>riginal</dt>
										<dd>他がやらないことへ挑戦し続け、唯一無二のカンパニーを目指す。</dd>
										<dt><span>P</span>rofessional</dt>
										<dd>「自信」と「誇り」、「謙虚さ」を持ち続け、プロとしての結果を出し続ける。</dd>
										<dt><span>R</span>esponsible</dt>
										<dd>全社員が責任感を持ち、努力し続け、社会に必要とされるカンパニーを目指す。</dd>
										<dt><span>O</span>bjective</dt>
										<dd>「勇気と覚悟」を持ち、目標達成に邁進する。</dd>
									</dl>
								</div>
								<!-- content end -->
								<?php echo makeRelational($rel_array) ?>
							</article>
						</main>
					</div><!-- mainWrap end -->
					<?php include( $DOC_ROOT . '/inc/side_inc.php');?>
				</div><!-- cont980 end -->
				<?php include( $DOC_ROOT . '/inc/footer_inc.php');?>
			</div><!-- contWrap end -->
		<?php include( $DOC_ROOT . '/inc/script_inc.php');?>
		<script type="text/javascript" src="/assets/js/company.js"></script>
	</div><!-- allWrap end -->
</body>
</html>

