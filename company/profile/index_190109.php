<?php
//必須ファイル読み込み
require_once('../../system/function.php');
//ページ設定
$str_h1 = '会社概要';
$str_dsc =  $SITE_NAME . 'の会社概要をご紹介します。';
$str_tit = '会社概要';
$str_kwd = '会社概要';

//関連リンク設定
$rel_array = array(
	'/company/message/' => '代表挨拶',
	'/company/philosophy/' => '企業理念',
	'/company/group/' => 'コプログループについて',
	'/company/history/' => '会社沿革'
);
?>
<!DOCTYPE html>
<html lang="ja">
<head>
	<?php include( $DOC_ROOT . '/inc/meta_inc.php');?>
	<link rel="stylesheet" type="text/css" href="/assets/css/company.css">
</head>
<body id="<?php echo $str_page_id; ?>">
	<div id="allWrap">
		<?php include( $DOC_ROOT . '/inc/header_inc.php');?>
			<div class="contWrap">
				<div class="cont980 ibWrap">
					<nav class="breadCrumb">
						<ul>
							<li><a href="/">ホーム</a></li>
							<li><a href="/company/"><i class="fa fa-chevron-right"></i>企業情報</a></li>
							<li><i class="fa fa-chevron-right"></i><?php echo $str_tit; ?></li>
						</ul>
					</nav>
					<h2 class="h2_line h2_short"><?php echo $str_tit; ?></h2>
					<div id="mainWrap" class="contMain">
						<main>
							<article>
								<!-- content -->
								<dl class="dl_profile">
									<dt>会社名</dt>
									<dd><?php echo $SITE_NAME; ?></dd>
								</dl>
								<dl class="dl_profile">
									<dt>代表者</dt>
									<dd>
									代表取締役社長<span class="pc-display">&nbsp;&nbsp;</span>&nbsp;&nbsp;&nbsp;　　　　　<br class="sp-display">清川 甲介
									</dd>
								</dl>
								<dl class="dl_profile">
									<dt>役員</dt>
                                    <dd>
									専務取締役&nbsp;事業本部長<span class="pc-display">&nbsp;&nbsp;&nbsp;&nbsp;</span>　　<br class="sp-display">小粥 哉澄<br><br class="sp-display">
									常務取締役&nbsp;管理本部長<span class="pc-display">&nbsp;&nbsp;&nbsp;&nbsp;</span>　　<br class="sp-display">齋藤 正彦<br><br class="sp-display">
									取締役&nbsp;財務経理部 部長<span class="pc-display">&nbsp;&nbsp;&nbsp;</span>　　<br class="sp-display">保浦 知生<br><br class="sp-display">
									取締役&nbsp;採用戦略本部長<span class="pc-display">&nbsp;&nbsp;&nbsp;&nbsp;</span>　　<br class="sp-display">越川 裕介<br><br class="sp-display">
									取締役&nbsp;人財開発本部長<span class="pc-display">&nbsp;&nbsp;&nbsp;&nbsp;</span>　　<br class="sp-display">向井 一浩<br><br class="sp-display">
									社外取締役<span class="pc-display">&nbsp;&nbsp;&nbsp;</span>　　　　　 　　  <br class="sp-display">中島 涼<br><br class="sp-display">
									社外取締役<span class="pc-display">&nbsp;&nbsp;&nbsp;</span>　　　　　　 　   <br class="sp-display">葉山 憲夫<br><br class="sp-display">
									常勤監査役<span class="pc-display">&nbsp;&nbsp;&nbsp;</span>　 　　 　　　　<br class="sp-display">星野 義明<br><br class="sp-display">
									社外監査役<span class="pc-display">&nbsp;&nbsp;&nbsp;</span>　　 　 　　　　<br class="sp-display">春馬 学<br><br class="sp-display">
									社外監査役<span class="pc-display">&nbsp;&nbsp;&nbsp;</span>　　 　　 　　　<br class="sp-display">大倉 淳<br>
									</dd>
								</dl>
								<dl class="dl_profile">
									<dt>本社</dt>
									<dd><span>〒450-6427&nbsp;&nbsp;</span><span>愛知県名古屋市中村区名駅三丁目28番12号&nbsp;&nbsp;</span><span>大名古屋ビルヂング27F</span><br><span>TEL <?php echo $TEL_NUMBER; ?>&nbsp;&nbsp;</span><span>FAX <?php echo $FAX_NUMBER; ?></span></dd>
								</dl>
								<dl class="dl_profile">
									<dt>従業員数</dt>
									<dd>1,667名（2018年9月末時点/グループ全体正社員・派遣社員含む）</dd>
								</dl>
								<dl class="dl_profile">
									<dt>資本金</dt>
									<dd>3,000万円</dd>
								</dl>
								<dl class="dl_profile">
									<dt>決算月</dt>
									<dd>3月</dd>
								</dl>
								<dl class="dl_profile">
									<dt>顧問弁護士</dt>
									<dd>アクシア法律事務所</dd>
								</dl>
								<dl class="dl_profile">
									<dt>顧問税理士</dt>
									<dd>税理士法人ブレインワン</dd>
								</dl>
								<dl class="dl_profile">
									<dt>グループ情報</dt>
									<dd>
										<div>
											<h5><a href="http://www.copro-e.co.jp/" target="_blank">株式会社コプロ・エンジニアード</a></h5>
											〒450-6427 愛知県名古屋市中村区名駅三丁目28番12号 大名古屋ビルヂング27F<br>
											TEL 052-589-2939 FAX 052-589-2938
										</div>
									</dd>
								</dl>
								<dl class="dl_profile">
									<dt>取引先銀行</dt>
									<dd>三井住友銀行／中京銀行／三菱UFJ銀行</dd>
								</dl>
								<!-- content end -->
								<?php echo makeRelational($rel_array) ?>
							</article>
						</main>
					</div><!-- mainWrap end -->
					<?php include( $DOC_ROOT . '/inc/side_inc.php');?>
				</div><!-- cont980 end -->
				<?php include( $DOC_ROOT . '/inc/footer_inc.php');?>
			</div><!-- contWrap end -->
		<?php include( $DOC_ROOT . '/inc/script_inc.php');?>
	</div><!-- allWrap end -->
</body>
</html>