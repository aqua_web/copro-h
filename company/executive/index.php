<?php
//必須ファイル読み込み
require_once('../../system/function.php');
//ページ設定
$str_h1 = '役員紹介';
$str_dsc = '役員の紹介をします';
$str_tit = '役員紹介';
$str_kwd = '役員紹介';


//関連リンク設定
$rel_array = array(
	'/company/philosophy/' => '企業理念',
	'/company/group/' => 'コプログループについて',
	'/company/history/' => '会社沿革',
	'/company/profile/' => '会社概要'
);

?>
<!DOCTYPE html>
<html lang="ja">
<head>
	<?php include( $DOC_ROOT . '/inc/meta_inc.php');?>
	<link rel="stylesheet" type="text/css" href="../../assets/css/company.css">
</head>
<body id="<?php echo $str_page_id; ?>">
	<div id="allWrap">
		<?php include( $DOC_ROOT . '/inc/header_inc.php');?>
			<div class="contWrap">
				<div class="cont980 ibWrap">
					<nav class="breadCrumb">
						<ul>
							<li><a href="/">ホーム</a></li>
							<li><a href="/company/"><i class="fa fa-chevron-right"></i>企業情報</a></li>
							<li><i class="fa fa-chevron-right"></i><?php echo $str_tit; ?></li>
						</ul>
					</nav>
					<h2 class="h2_line h2_short"><?php echo $str_tit; ?></h2>
					<div id="mainWrap" class="contMain">
						<main>
							<article>
								<!-- content -->
									<!-- content -->
									<div class="executive">  	
										<dl>
											<dt><img src="/assets/img/company/01.png" alt="代表取締役 清川甲介"></dt>
											<dd>
												<div>代表取締役社長 清川甲介</div>
											</dd>
										</dl>
										<dl>
											<dt><img src="/assets/img/company/02.png" alt="取締役 営業本部 小粥哉澄"></dt>
											<dd>
												<div>取締役 営業本部 小粥哉澄</div>
											</dd>
										</dl>
										<dl class="r">
											<dt><img src="/assets/img/company/03.png" alt="取締役 管理本部長 齋藤正彦"></dt>
											<dd>
												<div>取締役 管理本部長 齋藤正彦</div>
											</dd>
										</dl>
										<dl>
											<dt><img src="/assets/img/company/04.png" alt="取締役 採用戦略本部 越川 裕介"></dt>
											<dd>
												<div>取締役 採用戦略本部 越川 裕介</div>
											</dd>
										</dl>
                                        <dl>
											<dt><img src="/assets/img/company/05.png" alt="取締役 人財開発本部 向井一浩"></dt>
											<dd>
												<div>取締役 人財開発本部 向井一浩</div>
											</dd>
										</dl>
                                        <dl class="r">
											<dt><img src="/assets/img/company/06.png" alt="取締役 管理本部 副部長 河野 智之"></dt>
											<dd>
												<div>取締役 管理本部 副部長 河野 智之</div>
											</dd>
										</dl>
                                
							</div>					<!-- content end -->
								<?php echo makeRelational($rel_array) ?>
							</article>
						</main>
					</div><!-- mainWrap end -->
					<?php include( $DOC_ROOT . '/inc/side_inc.php');?>
				</div><!-- cont980 end -->
				<?php include( $DOC_ROOT . '/inc/footer_inc.php');?>
			</div><!-- contWrap end -->
		<?php include( $DOC_ROOT . '/inc/script_inc.php');?>
	</div><!-- allWrap end -->
</body>
</html>
