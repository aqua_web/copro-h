<?php
//必須ファイル読み込み
require_once('../../system/function.php');
//ページ設定
$str_dsc =  'コプログループについてご紹介します。';
$str_tit = 'コプログループについて';
$str_kwd = 'コプログループ';

//関連リンク設定
$rel_array = array(
	'/company/message/' => '代表挨拶',
	'/company/philosophy/' => '企業理念',
	'/company/history/' => '会社沿革',
	'/company/profile/' => '会社概要'
);

?>
<!DOCTYPE html>
<html lang="ja">
<head>
	<?php include( $DOC_ROOT . '/inc/meta_inc.php');?>
	<link rel="stylesheet" type="text/css" href="/assets/css/company.css">
</head>
<body id="<?php echo $str_page_id; ?>">
	<div id="allWrap">
		<?php include( $DOC_ROOT . '/inc/header_inc.php');?>
			<div class="contWrap">
				<div class="cont980 ibWrap">
					<nav class="breadCrumb">
						<ul>
							<li><a href="/">ホーム</a></li>
							<li><a href="/company/"><i class="fa fa-chevron-right"></i>企業情報</a></li>
							<li><i class="fa fa-chevron-right"></i><?php echo $str_tit; ?></li>
						</ul>
					</nav>
					<h2 class="h2_line h2_short"><?php echo $str_tit; ?></h2>
					<div id="mainWrap" class="contMain">
						<main>
							<article>
								<!-- content -->
									<div id="div_group">
										<div id="groupSlogan">
											<img src="/assets/img/company/pic_group01.png" alt="熱を、力に。"><br>
											<span>私たちは、日々の業務に</span><span>「熱」をもって真剣に取り組んでいます。</span><br>
											<span>熱とは、一生懸命だけを</span><span>意味する言葉ではありません。</span><br>
											<span>熱とは、私たちの原動力であり、</span><span>志であり、志事<span class="txtFootnote">※1</span>への覚悟です。</span><br>
											<span>急激な時代の変化に合わせて</span><span>経営の多角化が進み、</span><br>
											<span>事業規模が拡大したとしても</span><span>この信念が揺らぐことはありません。</span><br>
											<br>
											<span>人のため、社会のために、</span><span>常にこれ以上ない熱をそそぐ。</span><br>
											<span>たくさんの熱が大きな力となることで、</span><span>関わる全ての人たちに満足と感動を届け続けたい。</span><br>
											<span>私たちは、そう考えています。</span><br>
											<span>熱を力にすることで、</span><span>日々高みへと邁進していきます。</span><br>
											<a href="/company/philosophy/"><i class="fa fa-chevron-circle-right"></i>企業理念へ</a>
											<p>
												<span>※1　当社では、志や信念を持って、</span><span>世のため・人のために行う仕事を</span><span>「志事」と考えております。</span>
											</p>
										</div>
										<h3>コプログループについて</h3>
										<div id="groupOrg">
<!--
											<span><img src="/assets/img/company/pic_group02.jpg" alt="組織図"></span>
-->
											<p class="p_default">
												コプロ・ホールディングスは、建設エンジニア専門の人材派遣会社「コプロ・エンジニア―ド」を運営しています。<br>
												<br>
												グループ各社から発せられる「熱」をもって、未来の「熱」になる事業を展開していきます。
											</p>
										</div>
										<h3>グループ会社紹介</h3>
										<div class="groupComBox" id="companyE">
											<ul>
											<li>
												<span>建設エンジニア専門人材派遣</span>
												<h4>株式会社コプロ・エンジニアード</h4>
												<p>
													日本全国へ建設エンジニアの派遣を行っており、独自の組織体制で、人間力の高いエンジニアを数多くの現場に派遣しています。
												</p>
												<a href="http://www.copro-e.co.jp/" target="_blank"><i class="fa fa-chevron-circle-right"></i>コーポレートサイトへ</a>
											</li>
											<li>
												<img src="/assets/img/index/topGroup01.jpg" alt="株式会社コプロ・エンジニアード">
											</li>
										</div>
									</div>
								<!-- content end -->
								<?php echo makeRelational($rel_array) ?>
							</article>
						</main>
					</div><!-- mainWrap end -->
					<?php include( $DOC_ROOT . '/inc/side_inc.php');?>
				</div><!-- cont980 end -->
				<?php include( $DOC_ROOT . '/inc/footer_inc.php');?>
			</div><!-- contWrap end -->
		<?php include( $DOC_ROOT . '/inc/script_inc.php');?>
	</div><!-- allWrap end -->
</body>
</html>
