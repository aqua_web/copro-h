<?php
//必須ファイル読み込み
require_once('../../system/function.php');
//ページ設定
$str_h1 = '代表挨拶';
$str_dsc = '弊社代表、清川甲介からのご挨拶。';
$str_tit = '代表挨拶';
$str_kwd = '代表挨拶';


//関連リンク設定
$rel_array = array(
	'/company/philosophy/' => '企業理念',
	'/company/group/' => 'コプログループについて',
	'/company/history/' => '会社沿革',
	'/company/profile/' => '会社概要'
);

?>
<!DOCTYPE html>
<html lang="ja">
<head>
	<?php include( $DOC_ROOT . '/inc/meta_inc.php');?>
	<link rel="stylesheet" type="text/css" href="/assets/css/company.css">
</head>
<body id="<?php echo $str_page_id; ?>">
	<div id="allWrap">
		<?php include( $DOC_ROOT . '/inc/header_inc.php');?>
			<div class="contWrap">
				<div class="cont980 ibWrap">
					<nav class="breadCrumb">
						<ul>
							<li><a href="/">ホーム</a></li>
							<li><a href="/company/"><i class="fa fa-chevron-right"></i>企業情報</a></li>
							<li><i class="fa fa-chevron-right"></i><?php echo $str_tit; ?></li>
						</ul>
					</nav>
					<h2 class="h2_line h2_short"><?php echo $str_tit; ?></h2>
					<div id="mainWrap" class="contMain">
						<main>
							<article>
								<!-- content -->
								<div class="div_message clearfix">
									<div>
										<img src="/assets/img/company/pic_message01.jpg" alt="清川甲介">
									</div>
									<p class="p_default">
										<img src="/assets/img/company/pic_message01.jpg" alt="清川甲介">
										<span>熱を力に。</span>
										私たちコプログループは、「人を愛し、愛される会社」という経営理念のもと、創業以来お客様の期待に応えるサービスの提供に努めて参りました。<br />
										皆様のおかげをもちまして急成長を遂げていますが、その分企業としての社会的責任が非常に大きくなってきていると感じています。<br />
										<br />
										当社が目指しているのは、お客様・社員・社会といった、当社と関わるすべての人たちとともに発展していく、社会に必要とされる会社であり続けるということです。<br />
										<br />
										そのために、今後も継続的に全社一丸となって目標を達成するという姿勢を貫き、関わる全ての人への「感謝」の気持ちを忘れずお客様に提供できる技術やサービスの研鑚を重ねる所存です。<br />
										コプログループの全社員が、日々の志事に真剣に、志と覚悟をもって行動することで、一人ひとりのもつ熱を力に変え、創業時に掲げた理念をどこまでも追及して参ります。<br />
									</p>
								</div>
								<div class="div_messageSignature">
									株式会社コプロ・ホールディングス<br>
									代表取締役　清川甲介
									<br>
									<img src="/assets/img/company/pic_message02.jpg" alt="清川甲介">
								</div>
								<!-- content end -->
								<?php echo makeRelational($rel_array) ?>
							</article>
						</main>
					</div><!-- mainWrap end -->
					<?php include( $DOC_ROOT . '/inc/side_inc.php');?>
				</div><!-- cont980 end -->
				<?php include( $DOC_ROOT . '/inc/footer_inc.php');?>
			</div><!-- contWrap end -->
		<?php include( $DOC_ROOT . '/inc/script_inc.php');?>
	</div><!-- allWrap end -->
</body>
</html>
