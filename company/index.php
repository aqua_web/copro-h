<?php
//必須ファイル読み込み
require_once('../system/function.php');

//ページ設定
$str_dsc = $SITE_NAME . 'について。';
$str_tit = '企業情報';
$str_kwd = '企業情報';

//関連リンク設定
$rel_array = array(
	'/news/' => 'お知らせ',
);
?>
<!DOCTYPE html>
<html lang="ja">
<head>
	<?php include( $DOC_ROOT . '/inc/meta_inc.php');?>
</head>
<body id="<?php echo $str_page_id; ?>">
	<div id="allWrap">
		<?php include( $DOC_ROOT . '/inc/header_inc.php');?>
			<div class="contWrap">
				<div class="cont980 ibWrap">
					<nav class="breadCrumb">
						<ul>
							<li><a href="/">ホーム</a></li>
							<li><i class="fa fa-chevron-right"></i><?php echo $str_tit; ?></li>
						</ul>
					</nav>
					<h2 class="h2_line h2_short"><?php echo $str_tit; ?></h2>
					<div id="mainWrap" class="contMain">
						<main>
							<article>
								<!-- content -->
								<div class="cateTopNav">
									<a href="/company/message/">
										<i class="fa fa-chevron-circle-right"></i>代表挨拶
									</a>
									<a href="/company/profile/">
										<i class="fa fa-chevron-circle-right"></i>会社概要
									</a>
									<a href="/company/executive/">
										<i class="fa fa-chevron-circle-right"></i>役員紹介
									</a>
									<a href="/company/philosophy/">
										<i class="fa fa-chevron-circle-right"></i>企業理念
									</a>
									<a href="/company/group/">
										<i class="fa fa-chevron-circle-right"></i>コプログループについて
									</a>
									<a href="/company/history/">
										<i class="fa fa-chevron-circle-right"></i>会社沿革
									</a>
									<!--
									<a href="/company/ir/">
										<i class="fa fa-chevron-circle-right"></i>IR情報
									</a>
									-->
								</div>
								<!-- content end -->
								<?php echo makeRelational($rel_array) ?>
							</article>
						</main>
					</div><!-- mainWrap end -->
					<?php include( $DOC_ROOT . '/inc/side_inc.php');?>
				</div><!-- cont980 end -->
				<?php include( $DOC_ROOT . '/inc/footer_inc.php');?>
			</div><!-- contWrap end -->
		<?php include( $DOC_ROOT . '/inc/script_inc.php');?>
	</div><!-- allWrap end -->
</body>
</html>

