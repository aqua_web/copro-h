<?php
//必須ファイル読み込み
require_once('../system/function.php');

//ページ設定
$str_dsc = $SITE_NAME . 'のパブリシティ活動とメディア実績をご紹介します。';
$str_tit = 'メディア掲載情報';
$str_kwd = 'メディア掲載情報';

//wp読み込み
require_once( $DOC_ROOT . '/wp/wp-load.php');

/*
//関連リンク設定
$rel_array = array(
	'URL' => 'ページ名',
	'URL' => 'ページ名'
);
*/
?>
<!DOCTYPE html>
<html lang="ja">
<head>
	<?php include( $DOC_ROOT . '/inc/meta_inc.php');?>
	<link rel="stylesheet" type="text/css" href="/assets/css/publicity.css">
</head>
<body id="<?php echo $str_page_id; ?>">
	<div id="allWrap">
		<?php include( $DOC_ROOT . '/inc/header_inc.php');?>
			<div class="contWrap">
				<div class="cont980 ibWrap">
					<nav class="breadCrumb">
						<ul>
							<li><a href="/">ホーム</a></li>
							<li><i class="fa fa-chevron-right"></i><a href="/news/">お知らせ</a></li>
							<li><i class="fa fa-chevron-right"></i><?php echo $str_tit; ?></li>
						</ul>
					</nav>
					<h2 class="h2_line h2_short"><?php echo $str_tit; ?></h2>
					<div id="mainWrap" class="contMain">
						<main>
							<article>
								<!-- content-->

										<div class="div_publicityWrap">
  										<h3 class="h3_tit mgT30 mgB30">LEADERS' AWARDノミネート</h3>
											<ul class="ul_publicity">                                                
                                                <li>
                                                    <a href="http://www.leaders-award.jp/2018/entry9a2zb.html" target="_blank">
                                                        <img width="138" height="133" src="/assets/img/publicity/pic_publicity2018_01.png" class="attachment-news-publicity-thumb wp-post-image" alt="アワード用">
                                                    <h5>20万人の学生があこがれる<br class="pc">
                                                    リーダーズアワード</h5>
                                                    <p><span><i class="fa fa-chevron-circle-right"></i>2018年 インタビュー記事掲載<br>
                                                    </span></p>
                                                </a>													
                                                </li>
                                               <li>
                                                    <a href="http://www.leaders-award.jp/2017/entry9a2zb.html" target="_blank">
                                                        <img width="138" height="133" src="https://www.copro-e.co.jp/wp-content/uploads/2017/03/award2017.png" class="attachment-news-publicity-thumb wp-post-image" alt="アワード用">
                                                    <h5>20万人の学生があこがれる<br class="pc">
                                                    リーダーズアワード</h5>
                                                    <p><span><i class="fa fa-chevron-circle-right"></i>2017年 インタビュー記事掲載<br>
                                                    </span></p>
                                                </a>													
                                               </li>
                                                <li>
                                                    <a href="http://www.leaders-award.jp/2016/entry9a2zb.html" target="_blank"><img width="138" height="133" src="https://www.copro-e.co.jp/wp-content/uploads/2016/03/アワード用.png" class="attachment-news-publicity-thumb wp-post-image" alt="アワード用">
                                                    <h5>20万人の学生があこがれる<br class="pc">
                                                    リーダーズアワード</h5>
                                                    <p><span><i class="fa fa-chevron-circle-right"></i>2016年 インタビュー記事掲載<br>
                                                    </span></p>
                                                    </a>													
                                                </li>
                                                <!--
                                                <li>
													<a href="/news/img/2015.jpg" target="_blank">
													<img src="/assets/img/pic_publicity11.png" alt="">
													<h5>20万人の学生があこがれる<br class="pc">リーダーズアワード</h5>
													<span><i class="fa fa-chevron-circle-right"></i>2015年 ノミネート企業選出</span>
													</a>
												</li>
												<li>
													<a href="/news/img/2014.jpg" target="_blank">
													<img src="/assets/img/pic_publicity09.png" alt="">
													<h5>20万人の学生があこがれる<br class="pc">リーダーズアワード</h5>
													<span><i class="fa fa-chevron-circle-right"></i>2014年 ノミネート企業選出</span>
													</a>
												</li>
												<li>
													<a href="/news/img/2013.jpg" target="_blank">
													<img src="/assets/img/pic_publicity06.png" alt="">
													<h5>20万人の学生があこがれる<br class="pc">リーダーズアワード</h5>
													<span><i class="fa fa-chevron-circle-right"></i>2013年 ノミネート企業選出</span>
													</a>
												</li>
												<li>
													<img src="/assets/img/pic_publicity03.png" alt="">
													<h5>20万人の学生があこがれる<br class="pc">リーダーズアワード</h5>
													<span><i class="fa fa-chevron-circle-right"></i>2012年 ノミネート企業選出</span>
												</li>
                                                -->

											</ul>
                                                
								<h3 class="h3_tit mgT30 mgB30">記事掲載</h3>
											<ul class="ul_publicity">    
												<li>
													<img src="/assets/img/publicity/pic_publicity2018_07.png" alt="">
													<h5>週刊ダイヤモンド</h5>
													<span><i class="fa fa-chevron-circle-right"></i>2018年3月12日 記事掲載</span>
												</li>
												<li>
													<img src="/assets/img/publicity/pic_publicity2018_06.png" alt="">
													<h5>月刊人事マネジメント</h5>
													<span><i class="fa fa-chevron-circle-right"></i>2017年12月14日 記事掲載</span>
												</li>
												<li>
													<a href="http://diamond.jp/articles/-/145031" target="_blank"><img src="/assets/img/publicity/pic_publicity2018_05.png" alt="">
													<h5>ダイヤモンドONLINE</h5>
													<span><i class="fa fa-chevron-circle-right"></i>2017年10月31日 記事掲載</a></span>
												</li>

												<li>
													<a href="https://ashitanojinji.jp/event-report/2329" target="_blank"><img src="/assets/img/publicity/pic_publicity2018_04.png" alt="">
													<h5>あしたの人事online</h5>
													<span><i class="fa fa-chevron-circle-right"></i>2017年10月27日 記事掲載</a></span>
												</li>
												<li>
													<img src="/assets/img/publicity/pic_publicity2018_03.png" alt="">
													<h5>月刊BOSS</h5>
													<span><i class="fa fa-chevron-circle-right"></i>2017年10月23日 記事掲載</span>
												</li>
   											   <li>
													<a href="http://www.copro-e.co.jp/news/1284" target="_blank">
													<img src="/assets/img/publicity/pic_013.png" alt="">
													<h5>10周年記念書籍</h5>
													<span><i class="fa fa-chevron-circle-right"></i>2016年7月 記事掲載</span>
													</a>
  											   </li>
 											   <li>
													<a href="http://ps.nikkei.co.jp/myroad/keyperson/kiyokawa_kosuke/index.html" target="_blank">
													<img src="/assets/img/pic_publicity07.png" alt="">
													<h5>日経新聞電子版<br>～私の道しるべ～</h5>
													<span><i class="fa fa-chevron-circle-right"></i>2016年6月出演</span>
													</a>
  											   </li>

											</ul>


											<h3 class="h3_tit mgT30 mgB30">テレビ放送</h3>
											<ul class="ul_publicity">
    												<li>
  													<img src="/assets/img/publicity/pic_015.png" alt="">
  													<h5>MBS ブラマヨの質問ズケ！ 子どもに、何の習い事をさせるべき？</h5>
  													<span><i class="fa fa-chevron-circle-right"></i>2016年12月17日出演</span>
  													
  												</li>
    												<li>
  													<!--<a href="http://www.nhk.or.jp/nagoya/we/wmv/20160430_03.html" target="_blank">-->
  													<img src="/assets/img/pic_publicity14.png" alt="">
  													<h5>テレビ愛知 日曜なもんで！ キラキラ女子プロジェクト紹介</h5>
  													<span><i class="fa fa-chevron-circle-right"></i>2016年9月18日出演</span>
  													<!--</a>-->
  												</li>
    												<li>
													<!--<a href="http://www.nhk.or.jp/nagoya/we/wmv/20160430_03.html" target="_blank">-->
													<img src="/assets/img/pic_publicity12.png" alt="">
													<h5>NHK名古屋 ウィークエンド中部 ユニーク名刺特集</h5>
													<span><i class="fa fa-chevron-circle-right"></i>2016年4月30日出演</span>
													<!--</a>-->
												</li>
											</ul>
                                            <!--動画サイト掲載-->
											<h3 class="h3_tit mgT30 mgB30">動画サイト掲載</h3>
											<ul class="ul_publicity">
                                                <li>
													<!--a href="https://www.weekly-economist.com/" target="_blank"-->
													<img src="/assets/img/publicity/pic_publicity2018_02.png" alt="週刊エコノミスト">
													<h5>週刊エコノミスト</h5>
													<span><i class="fa fa-chevron-circle-right"></i>2018年3月出演</span>
													<!--</a>-->
												</li>
                                                <li>
													<!--a href="http://shigototecho-tv.jp/kiyokawa_kosuke-2/" target="_blank"-->
													<img src="/assets/img/pic_publicity_mx.png" alt="TOKYO MX">
													<h5>コプロで働く社員が、仕事に夢中になれる理由に迫る！</h5>
													<span><i class="fa fa-chevron-circle-right"></i>2017年4月22日出演</span>
													<!--</a>-->
												</li>
                                                <li>
													<a href="http://www.kenja.tv/index.php?c=detail&m=index&kaiinid=10658" target="_blank">
													<img src="/assets/img/pic_publicity01.png" alt="賢者.tv">
													<h5>アジア最大級の社長動画サイト</h5>
													<span><i class="fa fa-chevron-circle-right"></i>2016年6月出演</span>
													</a>
												</li>
												<li>
													<a href="http://youtu.be/Tl6uPVpiP8o" target="_blank">
													<img src="/assets/img/pic_publicity08.png" alt="">
													<h5>アジア最大級の社長動画サイト</h5>
													<span><i class="fa fa-chevron-circle-right"></i>2013年5月19日出演</span>
													</a>
												</li>
												<li>
													<a href="http://www.youtube.com/watch?v=igu-RJYfpSE&feature=youtu.be" target="_blank">
													<img src="/assets/img/pic_publicity04.png" alt="">
													<h5>学生が選ぶ働きたい企業</h5>
													<span><i class="fa fa-chevron-circle-right"></i>2012年10月27日出演</span>
													</a>
												</li>
												<li>
													<a href="http://www.youtube.com/watch?v=fG3dyLISz80&feature=youtu.be" target="_blank">
													<img src="/assets/img/pic_publicity02.png" alt="">
													<h5>ビジネス・経済専門チャンネル</h5>
													<span><i class="fa fa-chevron-circle-right"></i>2012年2月4日出演</span>
													</a>
												</li>
												<li>
													
													<img src="/assets/img/pic_publicity01.png" alt="賢者.tv">
													<h5>アジア最大級の社長動画サイト</h5>
													<span><i class="fa fa-chevron-circle-right"></i>2011年7月出演</span>
													
												</li>
											</ul>
										</div>
												
								<!-- content end -->
								<?php echo makeRelational($rel_array) ?>
							</article>
						</main>
					</div><!-- mainWrap end -->
					<?php include( $DOC_ROOT . '/inc/side_inc.php');?>
				</div><!-- cont980 end -->
				<?php include( $DOC_ROOT . '/inc/footer_inc.php');?>
			</div><!-- contWrap end -->
		<?php include( $DOC_ROOT . '/inc/script_inc.php');?>
	</div><!-- allWrap end -->
</body>
</html>