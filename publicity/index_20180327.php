<?php
//必須ファイル読み込み
require_once('../system/function.php');

//ページ設定
$str_dsc = $SITE_NAME . 'のパブリシティ活動とメディア実績をご紹介します。';
$str_tit = 'メディア掲載情報';
$str_kwd = 'メディア掲載情報';

//wp読み込み
require_once( $DOC_ROOT . '/wp/wp-load.php');

/*
//関連リンク設定
$rel_array = array(
	'URL' => 'ページ名',
	'URL' => 'ページ名'
);
*/
?>
<!DOCTYPE html>
<html lang="ja">
<head>
	<?php include( $DOC_ROOT . '/inc/meta_inc.php');?>
	<link rel="stylesheet" type="text/css" href="/assets/css/publicity.css">
</head>
<body id="<?php echo $str_page_id; ?>">
	<div id="allWrap">
		<?php include( $DOC_ROOT . '/inc/header_inc.php');?>
			<div class="contWrap">
				<div class="cont980 ibWrap">
					<nav class="breadCrumb">
						<ul>
							<li><a href="/">ホーム</a></li>
							<li><i class="fa fa-chevron-right"></i><a href="/news/">お知らせ</a></li>
							<li><i class="fa fa-chevron-right"></i><?php echo $str_tit; ?></li>
						</ul>
					</nav>
					<h2 class="h2_line h2_short"><?php echo $str_tit; ?></h2>
					<div id="mainWrap" class="contMain">
						<main>
							<article>
								<!-- content-->
								<h3 class="h3_tit mgT30 mgB30">記事掲載</h3>
								<ul class="ul_publicity">
									<li>
										<a href="http://www.copro-e.co.jp/news/1284" target="_blank">
											<img src="/assets/img/publicity/pic_013.png" alt="10周年記念書籍">
											<h5>10周年記念書籍</h5>
											<span><i class="fa fa-chevron-circle-right"></i>2016年7月 記事掲載</span>
										</a>
									</li>
								</ul>

								<h3 class="h3_tit mgT30 mgB30">テレビ放送</h3>
								<ul class="ul_publicity">
									<li>
  										<img src="/assets/img/publicity/pic_015.png" alt="">
  										<h5>MBS ブラマヨの質問ズケ！ 子どもに、何の習い事をさせるべき？</h5>
  										<span><i class="fa fa-chevron-circle-right"></i>2016年12月17日出演</span>
									</li>
								</ul>
								
								<!-- content end -->
								<?php echo makeRelational($rel_array) ?>
							</article>
						</main>
					</div><!-- mainWrap end -->
					<?php include( $DOC_ROOT . '/inc/side_inc.php');?>
				</div><!-- cont980 end -->
				<?php include( $DOC_ROOT . '/inc/footer_inc.php');?>
			</div><!-- contWrap end -->
		<?php include( $DOC_ROOT . '/inc/script_inc.php');?>
	</div><!-- allWrap end -->
</body>
</html>