<?php
//必須ファイル読み込み
require_once('./system/function.php');

if(isset($_GET['preview'])) {
	header("Location: /wp". $_SERVER["REQUEST_URI"]);
	exit;
}
//ページ設定
$str_dsc = '';
$str_tit = 'コプログループの管理・運営';
$str_kwd = '';

require_once( $DOC_ROOT . '/wp/wp-load.php');

//コプロ・ホールディングスからのお知らせ
$args = array(
	'posts_per_page' => 3,
	'orderby' => 'post_date',
	'order' => 'DESC',
	'post_type' => 'post',
	'post_status' => 'publish',
	'cat' => '1' 
);
$the_query = new WP_Query($args);

if ( $the_query->have_posts() ) {
	$str_holdings = '<dl class="dl_news"><dt><span class="newsLong">コプロ・ホールディングス</span><span class="newsShort">当社</span>からのお知らせ<a href="/category/holdings/">一覧へ<i class="fa fa-list-ul"></i></a></dt>';
	while ( $the_query->have_posts() ) {
		 $the_query->the_post();
		//記事情報取得
		$list_text = get_post_meta($post->ID, 'list_text', true );
		$detail_url = get_post_meta($post->ID, 'detail_url', true );
		$detail_check = get_post_meta($post->ID, 'detail_check', true );
		//html
		if(!empty($list_text)){//一覧用テキストが存在
		
			$str_body = $list_text;
			
		}else{
			$str_body = get_the_title();
			if(!empty($detail_check)){//詳細リンクが存在
				if(!empty($detail_url)){//別URL指定が存在
					if(preg_match('/copro-h.co.jp/',$detail_url)){
						$str_body .= '</span><span>詳しくは<a href="' . $detail_url .'">こちら</a>';
					}else{
						$str_body .= '</span><span>詳しくは<a href="' . $detail_url .'" target="_blank">こちら</a>';
					}
				}else{
					$str_body .= '</span><span>詳しくは<a href="' . get_permalink() .'">こちら</a>';
				}
			}
		}
		$str_holdings .= "<dd><span>" . get_the_time('Y.m.d') . "</span>\n";
		$str_holdings .= "<span>" . $str_body . "</span></dd>\n";
	} //endwhile
	$str_holdings .= '</dl>';
}
wp_reset_postdata();

//グループ会社からのお知らせ取得
$args = array(
	'posts_per_page' => 3,
	'orderby' => 'post_date',
	'order' => 'DESC',
	'post_type' => 'post',
	'post_status' => 'publish',
	'cat' => '-1'
);
$the_query = new WP_Query($args);

if ( $the_query->have_posts() ) {
	$str_group = '<dl><dt>グループ会社からのお知らせ<a href="/news_group/">一覧へ<i class="fa fa-list-ul"></i></a></dt>';
	while ( $the_query->have_posts() ) {
		 $the_query->the_post();
		//記事情報取得
		$list_text = get_post_meta($post->ID, 'list_text', true );
		$detail_url = get_post_meta($post->ID, 'detail_url', true );
		$detail_check = get_post_meta($post->ID, 'detail_check', true );
		$cat = get_the_category();
		$cat_name = '';
		if(is_array($cat)){
			foreach( $cat as $cate ) {
				$cat_name .= '<a class="wpCate" href="/category/' . $cate->category_nicename . '">' . $cate->cat_name . '</a>';
			}
		}
		//html
		if(!empty($list_text)){//一覧用テキストが存在
		
			$str_body = $list_text;
			
		}else{
			$str_body = get_the_title();
			if(!empty($detail_check)){//詳細リンクが存在
				if(!empty($detail_url)){//別URL指定が存在
					if(preg_match('/copro-h.co.jp/',$detail_url)){
						$str_body .= '</span><span>詳しくは<a href="' . $detail_url .'">こちら</a>';
					}else{
						$str_body .= '</span><span>詳しくは<a href="' . $detail_url .'" target="_blank">こちら</a>';
					}
				}else{
					$str_body .= '</span><span>詳しくは<a href="' . get_permalink() .'">こちら</a>';
				}
			}
		}
		$str_group .= "<dd><dl class=\"dl_default\"><dt>" . get_the_time('Y.m.d') . '</dt><dd>' . $cat_name . "</dd></dl>\n";
		$str_group .= "<span>" . $str_body . "</span></dd>\n";
	} //endwhile
	$str_group .= '</dl>';
}
wp_reset_postdata();
?>
<!DOCTYPE html>
<html lang="ja">
<head>
	<?php include( $DOC_ROOT . '/inc/meta_inc.php');?>
	<link rel="stylesheet" type="text/css" href="/assets/css/index.css">
</head>
<body id="<?php echo $str_page_id; ?>">
	<div id="allWrap">
		<?php include( $DOC_ROOT . '/inc/header_inc.php');?>
			<div class="contWrap">
				<div id="mainWrap">
					<main>
						<article>
							<div id="topMainVis" class="cont980">
								<img src="/assets/img/index/topMainVisCover.png" alt="COPRO">
								<ul>
									<li><img src="/assets/img/index/topMainVis01.jpg" alt="COPRO"></li>
									<li><img src="/assets/img/index/topMainVis02.jpg" alt="COPRO"></li>
									<li><img src="/assets/img/index/topMainVis04.jpg" alt="COPRO"></li>
									<li><img src="/assets/img/index/topMainVis05.jpg" alt="COPRO"></li>
									<li><img src="/assets/img/index/topMainVis05.jpg" alt="COPRO"></li>
								</ul>
							</div>
							<div id="topNav" class="cont1280">
								<a href="/company/group/">
									<div class="navImgCng"><img src="/assets/img/index/topNav01.jpg" alt="コプログループについて"><img src="/assets/img/index/topNav01Sp.jpg" alt="コプログループについて"></div>
									<dl>
										<dt>コプログループについて</dt>
										<dd>
											コプログループの考えと<br>これからの展望をご紹介いたします。
										</dd>
									</dl>
								</a>
								<a href="/company/">
									<div><img src="/assets/img/index/topNav02.jpg" alt="企業情報"></div>
									<dl>
										<dt>企業情報</dt>
										<dd>
											代表からのご挨拶や企業理念など<br>当社の情報をご覧いただけます。
										</dd>
									</dl>
								</a>
								<a href="/recruit/">
									<div><img src="/assets/img/index/topNav03.jpg" alt="採用情報"></div>
									<dl>
										<dt>採用情報</dt>
										<dd>
											グループ各社の募集要項など<br>採用情報をご紹介いたします。
										</dd>
									</dl>
								</a>
							</div>
							<div id="topInfo" class="cont1280">
								<div>
									<?php echo $str_holdings; ?>
								</div>
								<div>
									<?php echo $str_group; ?>
								</div>
							</div>
							
							<div id="topGroup">
								<div class="cont980">
									<h3>COPRO GROUP</h3>
									<ul>
										<li>
											<a class="nolineAnc" href="/company/group/#companyE">
												<img src="/assets/img/index/topGroup01.jpg" alt="株式会社コプロ・エンジニアード">
												<span><i class="fa fa-chevron-circle-right"></i>株式会社コプロ・エンジニアード</span>
											</a>
										</li>
									</ul>
								</div>
							</div>
							
							<div class="top10th">
								<ul>
									<li><img src="/assets/img/index/top10th.png" alt="おかげさまで創業10周年"></li>
									<li>
										<p>
											<span>おかげさまで</span><span>コプロは</span><span>創業10周年を迎えました。</span>
										</p>
										<p>
											<span>コプロ・エンジニアード設立から</span><span>ホールディングス体制に</span><span>移行までのこの10年間、</span>
											<span>売上や拠点数など、</span><span>確かな成長を遂げましたが、</span><br class="noneSp">
											<span>今後最も成長させるべき</span><span>顧客満足度を向上させるため、</span><br class="noneSp">
											<span>これからも、今まで以上に、</span><span>私たちの<span class="top10thFev">熱</span>を注いでいきます。</span>
										</p>
										<p>
											<span>10周年ロゴは、<span class="top10thFev">熱</span>を表す炎に、</span><span>節目の「10」をアレンジし、</span>
											<span>「0」の形は当社イメージキャラクターの</span><span>「コプ郎」の頭の形をモチーフにしています。</span>
											<span>その周囲には、勝利を意味する月桂樹を、</span><span>コプロのロゴで表現しました。</span><br>
											<span>炎のように、</span><span>この10年間の成長がさらに立ち昇る、</span><span>新たなコプロを目指して参ります。</span>
										</p>
									</li>
								</ul>
							</div>

						</article>
					</main>
				</div><!-- mainWrap end -->
				<?php include( $DOC_ROOT . '/inc/footer_inc.php');?>
			</div><!-- contWrap end -->
		<?php include( $DOC_ROOT . '/inc/script_inc.php');?>
		<script type="text/javascript" src="/assets/js/index.js"></script>
	</div><!-- allWrap end -->
</body>
</html>