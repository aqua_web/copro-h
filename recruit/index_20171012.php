<?php
//必須ファイル読み込み
require_once('../system/function.php');
//ページ設定
$str_dsc = $SITE_NAME . 'の採用情報をご紹介します。';
$str_tit = '採用情報';
$str_kwd = '採用情報';

//関連リンク設定
$rel_array = array(
	'/company/group/' => 'コプログループについて',
	'/company/profile/' => '会社概要'
);
?>
<!DOCTYPE html>
<html lang="ja">
<head>
	<?php include( $DOC_ROOT . '/inc/meta_inc.php');?>
	<link rel="stylesheet" type="text/css" href="/assets/css/recruit.css">
</head>
<body id="<?php echo $str_page_id; ?>">
	<div id="allWrap">
		<?php include( $DOC_ROOT . '/inc/header_inc.php');?>
			<div class="contWrap">
				<div class="cont980 ibWrap">
					<nav class="breadCrumb">
						<ul>
							<li><a href="/">ホーム</a></li>
							<li><i class="fa fa-chevron-right"></i><?php echo $str_tit; ?></li>
						</ul>
					</nav>
					<h2 class="h2_line h2_short"><?php echo $str_tit; ?></h2>
					<div id="mainWrap" class="contMain">
						<main>
							<article>
								<!-- content-->
								
								<div id="recruitTit"><img src="/assets/img/recruit/recruitTit01.png" alt="熱さは、力になる。"></div>
								<div id="recruitLead">
									<span>私たちコプログループは、</span><br class="onlySp"><span>私たちと一緒に</span><br class="noneSp">
									<span>熱をもって</span><span>仕事に取り組んでいただける方を募集しています。</span>
								</div>
								
								<ul id="ul_recruit">
									<li id="recruitE">
										<h3>
											<span>建設エンジニア専門人材派遣</span>
											株式会社コプロ・エンジニアード
										</h3>
										<dl>
											<dt>新卒採用</dt>
											<dd><i class="fa fa-chevron-right"></i><a href="http://www.copro-e.co.jp/recruitment/" target="_blank">営業系 事務系</a></dd>
											<dd><i class="fa fa-chevron-right"></i><a href="http://www.copro-e.co.jp/recruitment/" target="_blank">エンジニア系</a></dd>
										</dl>
										<dl>
											<dt>キャリア採用</dt>
											<dd><i class="fa fa-chevron-right"></i><a href="http://www.copro-e.co.jp/recruit/consulting/" target="_blank">人材コンサルティング営業職</a></dd>
											<!--<dd><i class="fa fa-chevron-right"></i><a href="http://www.copro-e.co.jp/recruit/coordinator/" target="_blank">リクルーティングコーディネーター</a></dd>-->
											<dd><i class="fa fa-chevron-right"></i><a href="http://www.copro-e.co.jp/recruit/cad/" target="_blank">CADトレーナー</a></dd>
											<dd><i class="fa fa-chevron-right"></i><a href="http://www.copro-e.co.jp/recruit/cs/" target="_blank">CS職</a></dd>
											<dd><i class="fa fa-chevron-right"></i>
												<a href="http://www.g-career.net/" target="_blank">エンジニア系</a>
												<div><span>当社が運営する建設業界専門求人サイト</span><span>「現場監督キャリアネット」へ</span></div>
											</dd>
										</dl>
									</li>
								</ul>
								
								<!-- content end -->
								<?php echo makeRelational($rel_array) ?>
							</article>
						</main>
					</div><!-- mainWrap end -->
					<?php include( $DOC_ROOT . '/inc/side_inc.php');?>
				</div><!-- cont980 end -->
				<?php include( $DOC_ROOT . '/inc/footer_inc.php');?>
			</div><!-- contWrap end -->
		<?php include( $DOC_ROOT . '/inc/script_inc.php');?>
	</div><!-- allWrap end -->
</body>
</html>