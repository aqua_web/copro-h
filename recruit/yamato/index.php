<?php
//必須ファイル読み込み
require_once('../../system/function.php');
//ページ設定
$str_dsc = $SITE_NAME . 'の採用情報をご紹介します。';
$str_tit = '募集要項 / 株式会社大和';
$str_kwd = '募集要項';

//関連リンク設定
$rel_array = array(
	'/recruit/' => '採用情報トップ'
);

$str_page_id = 'pageRecuritY';
?>
<!DOCTYPE html>
<html lang="ja">
<head>
	<?php include( $DOC_ROOT . '/inc/meta_inc.php');?>
	<link rel="stylesheet" type="text/css" href="/assets/css/recruit.css">
</head>
<body id="<?php echo $str_page_id; ?>">
	<div id="allWrap">
		<?php include( $DOC_ROOT . '/inc/header_inc.php');?>
			<div class="contWrap">
				<div class="cont980 ibWrap">
					<nav class="breadCrumb">
						<ul>
							<li><a href="/">ホーム</a></li>
							<li><i class="fa fa-chevron-right"></i><a href="/recruit/">採用情報</a></li>
							<li><i class="fa fa-chevron-right"></i><?php echo $str_tit; ?></li>
						</ul>
					</nav>
					<h2 class="h2_line h2_short"><?php echo $str_tit; ?></h2>
					<div id="mainWrap" class="contMain">
						<main>
							<article>
								<!-- content-->
								<div class="requireInfo">
									<div class="verticalLine"></div>
									<dl>
										<dt>職種</dt><dd>管理栄養士・栄養士</dd>
									</dl>
									<dl>
										<dt>仕事内容</dt>
										<dd>
											① 献立作成、栄養マネジメント、食材発注、検品、事務作業<span> （書類管理、電話応対、その他庶務）</span><br>
											② 献立作成、調理、事務作業<span> （書類管理、電話応対、その他庶務）</span>
										</dd>
									</dl>
									<dl>
										<dt>必要資格</dt><dd>栄養士</dd>
									</dl>
									<dl>
										<dt>給与</dt><dd>①、②共通　時給1,200円～</dd>
									</dl>
									<dl>
										<dt>勤務地</dt>
										<dd>
											〒456-0032　<span>愛知県名古屋市熱田区三本松町4-16</span>
										</dd>
									</dl>
									<dl>
										<dt>勤務時間</dt>
										<dd>
											<span>① 8:00～17:00</span><span> （勤務時間相談可）</span><br>
											<span>② 5:30～14:00</span> / <span>6:30～15:30</span><span> （勤務時間相談可）</span>
										</dd>
									</dl>
									<dl>
										<dt>休日</dt><dd>土・日・祝日</dd>
									</dl>
									<dl>
										<dt>保険</dt><dd><span>健康保険・</span><span>雇用保険・</span><span>労災保険・</span><span>厚生年金保険</span></dd>
									</dl>
									<dl>
										<dt>諸手当</dt><dd>交通費規定により支給</dd>
									</dl>
								</div>
								<p class="p_requireInfo p_default">
									ご応募いただける方は下記までご連絡ください。<br>
									担当：青木 一夫<br>
									<span><a href="tel:0528818681" class="telLink"><i class="fa fa-phone"></i>052-881-8681</a></span>
									（受付時間：12:00～17:00）
								</p>
								<!-- content end -->
								<?php echo makeRelational($rel_array) ?>
							</article>
						</main>
					</div><!-- mainWrap end -->
					<?php include( $DOC_ROOT . '/inc/side_inc.php');?>
				</div><!-- cont980 end -->
				<?php include( $DOC_ROOT . '/inc/footer_inc.php');?>
			</div><!-- contWrap end -->
		<?php include( $DOC_ROOT . '/inc/script_inc.php');?>
		<script type="text/javascript" src="/assets/js/recruit.js"></script>
	</div><!-- allWrap end -->
</body>
</html>