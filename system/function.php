<?php
//ルートディレクトリ定義
if( preg_match('/copro-h.dev/', $_SERVER['HTTP_HOST']) ){
	$DOC_ROOT = 'E:/xampp/htdocs/sites/copro-h';
	define('DOC_ROOT','E:/xampp/htdocs/sites/copro-h');
}elseif( preg_match('/copro-h.tamagoinc.com/', $_SERVER['HTTP_HOST']) ){
	$DOC_ROOT = '/home/sites/heteml/users/t/a/m/tamagoinc/web/client/copro-h';
	define('DOC_ROOT','/home/sites/heteml/users/t/a/m/tamagoinc/web/client/copro-h');
}else{
	$DOC_ROOT = $_SERVER['DOCUMENT_ROOT'];
	define('DOC_ROOT',$_SERVER['DOCUMENT_ROOT']);
}

error_reporting(E_ALL ^ E_NOTICE);

//ページid初期値設定、カテゴリ変数生成
$dir_array = explode('/',$_SERVER['REQUEST_URI']);
if( empty($dir_array[1])){
	$str_page_id = 'pageIndex';
	$str_cate = 'index';
}else{
	$str_page_id = 'page' . ucfirst($dir_array[1]);
	$str_cate = $dir_array[1];
}

//関連ページ配列初期化
$rel_array = array();

//レスポンシブ制御初期化
$no_res_flag = false;

//共通変数、定数定義
$SITE_URL = 'https://www.copro-h.co.jp/';
define('SITE_URL','https://www.copro-h.co.jp/');
$SITE_NAME = '株式会社コプロ・ホールディングス';
define('SITE_NAME','株式会社コプロ・ホールディングス');
$TEL_NUMBER = '052-589-3066';
define('TEL_NUMBER','052-589-3066');
$TEL_NUMBER_S = '0525893066';
define('TEL_NUMBER_S','0525893066');
$FAX_NUMBER = '052-589-3067';
define('FAX_NUMBER','052-589-3067');

//関連リンク出力関数
function makeRelational($rel_array){
	
	if(!empty($rel_array)){
		$rel_link = '<dl class="relNav"><dt>関連リンク</dt>';
		foreach ( $rel_array as $rel_anc => $rel_name ){
			$rel_link .= '<dd><a href="' . $rel_anc . '"><i class="fa fa-chevron-right"></i>' . $rel_name . '</a></dd>';
		}
		$rel_link .= '</dl>' . "\n";
		return $rel_link;
	}
}

// デバッグ用の変数表示関数
function p(&$var, $title = ''){
	echo _preprint_r($var, $title);
}

function p_r(&$var, $title = ''){
	echo _preprint_r($var, $title);
}

function &_preprint_r(&$var, $title = ''){
	$html = '<table>';
	if ($title){
		$html .= "<tr><th align=\"left\">$title:</th></tr>";
	}
	$html .= '<tr><td><pre>';
	$html .= htmlspecialchars(print_r($var, true));
	$html .= "</pre></td></tr></table>\n";
	
	return $html;
}

?>