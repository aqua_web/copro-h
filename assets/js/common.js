// JavaScript Document
$(function(){
	
	//タッチデバイス対応判定
	var touch_flag = false;
	if (window.ontouchstart === null) {
		touch_flag = true;
	}  
	
	//電話リンク処理
	$('.telLink').on('click',function(){
		if(!touch_flag){
			return false;
		}
	});

	//SPカテゴリ開閉
	$('#sd_nav').find('dt').on('click',function(){
		$(this).children('i').toggle();
		if ($(this).next('dd').is(":hidden")){
			$(this).next('dd').slideDown('slow');
		}else{
			$(this).next('dd').slideUp('slow');
		}
		return false;
	});
	
	//フッターメニューカテゴリ開閉
	$('.bigFooter').find('dt').on('click',function(){
		if ( $(window).width() < 670 ) {
			if ($(this).next('dd').is(":hidden")){
				$(this).next('dd').slideDown('slow');
			}else{
				$(this).next('dd').slideUp('slow');
			}
			return false;
		}
	});
	
	//ページ内アンカー
	$('a[href^=#]').on('click', function() {
		var speed = 400;
		var href= $(this).attr("href");
		var target = $(href == "#" || href == "" ? 'html' : href);
		var position = target.offset().top - $('#headWrap').outerHeight();
		$('body,html').animate({scrollTop:position}, speed, 'swing');
		return false;
	});
	
	var resize_timer = false;
	$(window).resize(function() {
		if (resize_timer !== false) {
			clearTimeout(resize_timer);
		}
		resize_timer = setTimeout(function() {
			$('.bigFooter').find('dd').each(function() {
				if ( $(window).width() < 670 ) {
					$(this).css('display', 'none');
				}else{
					$(this).css('display', 'block');
				}
			});
		}, 200);
	});
	
});

window.onload = function() {//DOM構築後の処理
};


//左右のボックスの高さをそろえる関数
function dl_resize(target){
	$(target).children('dt').each(function(){
		dtH = 0;
		ddH = 0;
		$(this).css('height','inherit');
		$(this).next('dd').css('height','inherit');
		var dtH = $(this).outerHeight();
		var ddH = $(this).next('dd').outerHeight();
		if( dtH > ddH ){
			$(this).next('dd').css('height',dtH);
		}else if( dtH < ddH ){
			$(this).css('height',ddH);
		}
	});
}
