// JavaScript Document
$(function(){
	mainVis1();
});

function moveVisual( num, posX, posY, time, callback ){
	var target = $('#topMainVis').find('li:nth-child(' + num + ')');
	target.css({'left': -1*posX + '%','top': -1*posY + '%'})
	target.fadeIn(1000);
	target.animate({'left': 0,'top': 0},time,function(){
		$(this).fadeOut(1000);
		callback();
	});
}

function mainVis1(){
	moveVisual( 1, 0, 14, 6000, function(){
		mainVis2();
	});
}

function mainVis2(){
	moveVisual( 2, 6, 0, 6000, function(){
		mainVis3();
	});
}

function mainVis3(){
	moveVisual( 3, -6, 0, 6000, function(){
		mainVis4();
	});
}

function mainVis4(){
	moveVisual( 4, 0, -14, 6000, function(){
		mainVis5();
	});
}


function mainVis5(){
	$('#topMainVis').find('li:nth-child(5)').fadeIn(6000, function(){
		setTimeout(function(){
			$('#topMainVis').find('li:nth-child(5)').fadeOut(1000);
			mainVis1();
		},2000);
	});
}