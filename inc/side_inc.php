<?php
// E_NOTICEエラー以外出力する
error_reporting(E_ALL ^ E_NOTICE);
$refArray = explode('/',$_SERVER['HTTP_REFERER']);
$uriArray = explode('/',$_SERVER['REQUEST_URI']);
$menu_array = array();
$bnr_flag = false;


//ページIDごとのナビゲーション設定
if($str_page_id == 'pageCompany'){//企業情報ページ

	$menu_array = array(
		'/company/' => '企業情報トップ',
		'/company/message/' => '代表挨拶',
		'/company/philosophy/' => '企業理念',
		'/company/group/' => 'コプログループについて',
		'/company/history/' => '会社沿革',
		'/company/profile/' => '会社概要',
		#'/company/ir/' => 'IR情報'
	);
	
}elseif($str_page_id == 'pageNews' || $str_page_id == 'pagePublicity'){//お知らせページ

	$menu_array['/news/'] = 'お知らせ一覧';
	//年別ナビ
	$nowYear = date('Y');
	while( 2016 <= $nowYear){
		$menu_array['/date/' . $nowYear . '/' ] = $nowYear . '年';
		$nowYear--; 
	}
	//カテゴリー別ナビ
	$args = array(
		'orderby' => 'id',
		'order' => 'ASC'
	);
	$cat_all = get_categories($args);
	foreach($cat_all as $cat){
		$menu_array['/category/' . $cat->slug . '/'] = $cat->name;
	}
	$menu_array['/publicity/'] = 'メディア掲載情報';
	wp_reset_query();
	
}elseif($str_page_id == 'pageRecuritY'){//採用情報（大和）ページ

	$menu_array = array(
		'/recruit/' => '採用情報トップ',
		'/recruit/yamato/' => '株式会社大和'
	);
	
} elseif( $str_page_id == 'pagePrivacy' || $str_page_id == 'pageSitemap' || $str_page_id == 'pageRecruit' || $str_page_id == 'page404' ){//バナーのみ表示するページ

	$bnr_flag = true;
	
}else{//指定なし
	
	$menu_array = '';
	
}
//ナビゲーション追加バナー
$cate_nav_add = '<ul class="cateNavAdd">';
$cate_nav_add .= '<li><a href="http://www.copro-e.co.jp/" target="_blank"><img src="/assets/img/side/side_copro_e.jpg" alt="株式会社コプロ・エンジニアード"></a></li>';
$cate_nav_add .= '</ul>';

//HTML出力部分生成　
if(!empty($menu_array)){
	echo '<aside class="cateNavWrap"><ul class="cateNav">';
	foreach ( $menu_array as $menu_anc => $menu_name ){
		if($menu_anc == $_SERVER["REQUEST_URI"]){//現在のページ
			echo '<li class="cateNavAct">' . $menu_name . '</li>';
		}else{
			echo '<li><a href="' . $menu_anc . '"><i class="fa fa-chevron-right"></i>' . $menu_name . '</a></li>';
		}
	}
	echo '</ul>' . $cate_nav_add . '</aside>' . "\n";

}elseif($bnr_flag == true ){//バナーのみ表示するページ

	echo '<aside class="cateNavWrap">';
	echo $cate_nav_add . '</aside>' . "\n";

}

?>