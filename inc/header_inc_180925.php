<header>
			<div id="headWrap">
				<div id="headCont" class="cont980">
					<div id="headLogo">
						<a href="/"><img src="/assets/img/h1Logo.png" alt="ホーム" /></a>
					</div>
					<div id="headNav">
						<h1><?php echo $str_tit; ?><span><?php echo $SITE_NAME; ?></span></h1>
						<nav>
							<ul>
								<li><a href="/contact/" class="nolineAnc">お問い合わせ</a></li>
								<li><a href="/privacy/" class="nolineAnc">プライバシーポリシー</a></li>
								<li><a href="/sitemap/" class="nolineAnc"><span>サイトマップ</span></a></li>
							</ul>
							<ul>
								<li><a href="/news/" class="nolineAnc">お知らせ</a></li>
								<li><a href="/company/group/" class="nolineAnc">コプログループについて</a></li>
								<li><a href="/company/" class="nolineAnc"><span>企業情報</span></a></li>
								<li><a href="/recruit/" class="nolineAnc"><span>採用情報</span></a></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
			<div id="headNavTab">
				<div id="sd-trigger">
					<span></span>
					<div>MENU</div>
				</div>
				<nav id="sd_nav">
					<div class="close"><span></span></div>
					<ul>
						<li><a href="/"><i class="fa fa-chevron-circle-right"></i>ホーム</a></li>
						<li><a href="/news/"><i class="fa fa-chevron-circle-right"></i>お知らせ</a></li>
						<li><a href="/company/group/"><i class="fa fa-chevron-circle-right"></i>コプログループについて</a></li>
					</ul>
					<dl>
						<dt><i class="fa fa-chevron-circle-down"></i><i class="fa fa-chevron-circle-up"></i>企業情報</dt>
						<dd>
							<a href="/company/message/"><i class="fa fa-chevron-right"></i>代表挨拶</a>
							<a href="/company/philosophy/"><i class="fa fa-chevron-right"></i>企業理念</a>
							<a href="/company/history/"><i class="fa fa-chevron-right"></i>会社沿革</a>
							<a href="/company/profile/"><i class="fa fa-chevron-right"></i>会社概要</a>
							<!-- <a href="/company/ir/"><i class="fa fa-chevron-right"></i>IR情報</a> -->
						</dd>
					</dl>
					<ul>
						<li><a href="/recruit/"><i class="fa fa-chevron-circle-right"></i>採用情報</a></li>
						<li><a href="/contact/"><i class="fa fa-chevron-circle-right"></i>お問い合わせ</a></li>
						<li><a href="/privacy/"><i class="fa fa-chevron-circle-right"></i>プライバシーポリシー</a></li>
						<li><a href="/sitemap/"><i class="fa fa-chevron-circle-right"></i>サイトマップ</a></li>
					</ul>
				</nav>
			</div>
		</header>
