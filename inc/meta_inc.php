<meta charset="UTF-8">
	<meta name="format-detection" content="telephone=no" />
	<meta name="description" content="<?php echo $str_dsc; ?><?php echo $SITE_NAME; ?>はコプログループの管理・運営を行なっています。建設エンジニア専門人材派遣のコプロ・エンジニアードをはじめ、「熱」をもった事業を展開しています。">
	<meta name="keywords" content="<?php echo $str_kwd; ?>,コプロ・エンジニアード,コプロ・ホールディングス,コプロ・ソリューションズ,大和,建設エンジニア,飲食">
	<meta name="author" content="<?php echo $SITE_NAME; ?>">
	<?php if($no_res_flag === false){ echo '<meta name="viewport" content="width=device-width,initial-scale=1.0">'; } ?>
	<meta property="og:title" content="<?php echo $str_tit; ?>">
	<meta property="og:description" content="<?php echo $str_dsc; ?><?php echo $SITE_NAME; ?>はコプログループの管理・運営を行なっています。建設エンジニア専門人材派遣のコプロ・エンジニアードをはじめ、「熱」をもった事業を展開しています。">
	<meta property="og:url" content="<?php echo 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['PHP_SELF']; ?>">
	<meta property="og:image" content="<?php echo $SITE_URL; ?>ogimage.png">
	<meta property="og:site_name" content="<?php echo $SITE_NAME; ?>">
	<meta property="og:type" content="website"/>
	<link rel="shortcut icon" href="<?php echo $SITE_URL; ?>favicon.ico">
	<link rel="image_src" href="<?php echo $SITE_URL; ?>ogimage.png">
	<link rel="apple-touch-icon" href="<?php echo $SITE_URL; ?>ogimage.png">
	<link rel="stylesheet" type="text/css" href="/assets/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/smartDrawer.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/common.css">
	<title><?php if(!empty($str_tit)){ echo $str_tit . ' | ' ;} ?><?php echo $SITE_NAME; ?></title>
