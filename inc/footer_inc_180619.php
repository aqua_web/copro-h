<footer>
			<div id="woman" class="sp"><img src="/assets/img/womanenpower.png"/></div>
			<div id="secured"><script type="text/javascript" src="https://seal.websecurity.norton.com/getseal?host_name=www.copro-h.co.jp&amp;size=L&amp;use_flash=NO&amp;use_transparent=YES&amp;lang=ja"></script></div>
			<div class="footMoveAnc cont980">
				<ul>
					<li><a href="#" onclick="javascript:window.history.back(-1);return false;"><i class="fa fa-arrow-left"></i><span>前のページに戻る</span></a></li>
					<li><a href="#"><span>ページの先頭へ</span><i class="fa fa-arrow-up"></i></a></li>
				</ul>
			</div>
			<div class="footWrap">
				<div class="bigFooter">
					<div id="woman" class="pc"><img src="/assets/img/womanenpower.png"/></div>
					<ul>
						<li><a href="/"><i class="fa fa-chevron-circle-right"></i>ホーム</a></li>
						<li><a href="/company/group/"><i class="fa fa-chevron-circle-right"></i>コプログループについて</a></li>
						<li><a href="/news/"><i class="fa fa-chevron-circle-right"></i>お知らせ</a></li>
						<li><a href="/publicity/"><i class="fa fa-chevron-circle-right"></i>メディア掲載情報</a></li>
					</ul>
					<dl class="footNavShort">
						<dt><a href="/company/"><i class="fa fa-chevron-circle-right"></i><i class="fa fa-chevron-circle-down"></i>企業情報</a></dt>
						<dd>
							<a href="/company/message/"><i class="fa fa-chevron-right"></i>代表挨拶</a>
							<a href="/company/philosophy/"><i class="fa fa-chevron-right"></i>企業理念</a>
							<a href="/company/history/"><i class="fa fa-chevron-right"></i>会社沿革</a>
							<a href="/company/profile/"><i class="fa fa-chevron-right"></i>会社概要</a>
							<!-- <a href="/company/ir/"><i class="fa fa-chevron-right"></i>IR情報</a> -->
						</dd>
					</dl>
					<dl class="footNavLong">
						<dt><a href="/recruit/"><i class="fa fa-chevron-circle-right"></i><i class="fa fa-chevron-circle-down"></i>採用情報</a></dt>
						<dd>
							<a href="/recruit/#recruitE"><i class="fa fa-chevron-right"></i>株式会社コプロ・エンジニアード</a>
							<!--<a href="/recruit/#recruitS"><i class="fa fa-chevron-right"></i>株式会社コプロ・ソリューションズ</a>-->
						</dd>
					</dl>
					<ul>
						<li><a href="/contact/"><i class="fa fa-chevron-circle-right"></i>お問い合わせ</a></li>
						<li><a href="/privacy/"><i class="fa fa-chevron-circle-right"></i>プライバシーポリシー</a></li>
						<li><a href="/sitemap/"><i class="fa fa-chevron-circle-right"></i>サイトマップ</a></li>
					</ul>
				</div>
				<div class="footCopyWrap">
					<div class="footCopy cont980">
						<p>Copyright&nbsp;&copy;&nbsp;COPRO-HOLDINGS.&nbsp;Co.,&nbsp;Ltd.&nbsp;All&nbsp;right&nbsp;reserved.</p>
					</div>
				</div>
			</div>
		</footer>
