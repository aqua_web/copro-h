<?php
//必須ファイル読み込み
require_once('./../system/function.php');
//ページ設定
$str_dsc = $SITE_NAME . '公式HPのサイトマップです。';
$str_tit = 'サイトマップ';
$str_kwd = 'サイトマップ';
?>
<!DOCTYPE html>
<html lang="ja">
<head>
	<?php include( $DOC_ROOT . '/inc/meta_inc.php');?>
	<link rel="stylesheet" type="text/css" href="/assets/css/sitemap.css">
</head>
<body id="<?php echo $str_page_id; ?>">
	<div id="allWrap">
		<?php include( $DOC_ROOT . '/inc/header_inc.php');?>
			<div class="contWrap">
				<div class="cont980 ibWrap">
					<nav class="breadCrumb">
						<ul>
							<li><a href="/">ホーム</a></li>
							<li><i class="fa fa-chevron-right"></i><?php echo $str_tit; ?></li>
						</ul>
					</nav>
					<h2 class="h2_line h2_short"><?php echo $str_tit; ?></h2>
					<div id="mainWrap" class="contMain">
						<main>
							<article>
								<!-- content -->
								<div class="div_sitemapBox">
									
									<ul>
										<li><a href="/">ホーム</a></li>
										<li><a href="/news/">お知らせ</a></li>
									</ul>
									<ul>
										<li><a href="/company/group/">コプログループについて</a></li>
										<li><a href="/recruit/">採用情報</a></li>
									</ul>
									<dl>
										<dt><a href="/company/"></i>企業情報</a></dt>
										<dd>
											<a href="/company/message/">代表挨拶</a><br>
											<a href="/company/profile/">会社概要</a><br>
											<a href="/company/executive/">役員紹介</a><br>
											<a href="/company/philosophy/">企業理念</a><br>
											<a href="/company/history/">会社沿革</a><br>
											<!-- <a href="/company/ir/">IR情報</a> -->
										</dd>
									</dl>
									<ul>
										<li><a href="/contact/">お問い合わせ</a></li>
										<li><a href="/privacy/">プライバシーポリシー</a></li>
										<li><a href="/sitemap/">サイトマップ</a></li>
									</ul>
								</div>
								<!-- content end -->
								<?php echo makeRelational($rel_array) ?>
							</article>
						</main>
					</div><!-- mainWrap end -->
					<?php include( $DOC_ROOT . '/inc/side_inc.php');?>
				</div><!-- cont980 end -->
				<?php include( $DOC_ROOT . '/inc/footer_inc.php');?>
			</div><!-- contWrap end -->
		<?php include( $DOC_ROOT . '/inc/script_inc.php');?>
	</div><!-- allWrap end -->
</body>
</html>

