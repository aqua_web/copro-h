<?php
//必須ファイル読み込み
require_once('../system/function.php');

get_header();


$yearQuery = get_query_var( 'year', '' );
$yearTitle = '';
if(!empty($yearQuery)){
    $yearTitle = $yearQuery . '年';
    $yearTitleAfterTitle = ' / ' . $yearQuery . '年';
}

//ページ設定
$str_dsc = 'コプログループからのお知らせ';
$str_tit = 'お知らせ / ' . $yearTitle;
$str_kwd = 'お知らせ,' . $yearTitle;
$str_page_id = 'pageNews';

//カテゴリーナビ
$args = array(
	'orderby' => 'id',
	'order' => 'ASC'
);
$cat_all = get_categories($args);
$cat_nav = '<ul id="newsCateNav">';
foreach($cat_all as $cat){
	$cat_nav .= '<li><a href="/category/' . $cat->slug . '/">' . $cat->name . '</a></li>';
}
$cat_nav .= '</ul>';
wp_reset_query();

?>
<!DOCTYPE html>
<html lang="ja">
<head>
	<?php include( $DOC_ROOT . '/inc/meta_inc.php');?>
	<link rel="stylesheet" type="text/css" href="/assets/css/news.css">
</head>
<body id="<?php echo $str_page_id; ?>">
	<div id="allWrap">
		<?php include( $DOC_ROOT . '/inc/header_inc.php');?>
			<div class="contWrap">
				<div class="cont980 ibWrap">
					<nav class="breadCrumb">
						<ul>
							<li><a href="/">ホーム</a></li>
							<li><i class="fa fa-chevron-right"></i><a href="/news/">お知らせ</a></li>
							<li><i class="fa fa-chevron-right"></i><?php echo $yearTitle; ?></li>
						</ul>
					</nav>
					<h2 class="h2_line h2_short"><?php echo $str_tit; ?></h2>
					<div id="mainWrap" class="contMain">
						<main>
							<article>
								<!-- content-->
								<?php if ( have_posts()){

									$str_dl = '<dl class="dl_news">';
						
									// Start the Loop.
									while ( have_posts() ){
										the_post();
						
										//記事情報取得
										$list_text = get_post_meta($post->ID, 'list_text', true );
										$detail_url = get_post_meta($post->ID, 'detail_url', true );
										$detail_check = get_post_meta($post->ID, 'detail_check', true );
										$cat = get_the_category();
										$cat_name = '';
										if(is_array($cat)){
											foreach( $cat as $cate ) {
												$cat_name .= '<a href="/category/' . $cate->category_nicename . '">' . $cate->cat_name . '</a>';
											}
										}
										
										//html
										if(!empty($list_text)){//一覧用テキストが存在
										
											$str_body = $list_text;
											
										}else{
											$str_body = get_the_title();
											if(!empty($detail_check)){//詳細リンクが存在
												if(!empty($detail_url)){//別URL指定が存在
													if(preg_match('/copro-h.co.jp/',$detail_url)){
														$str_body .= '<br>詳しくは<a href="' . $detail_url .'">こちら</a>';
													}else{
														$str_body .= '<br>詳しくは<a href="' . $detail_url .'" target="_blank">こちら</a>';
													}
												}else{
													$str_body .= '<br>詳しくは<a href="' . get_permalink() .'">こちら</a>';
												}
											}
										}
										
										$str_dl .= "<dt><dl class=\"dl_default\"><dt>" . get_the_time('Y.m.d') . '</dt><dd>' . $cat_name . "</dd></dl></dt>\n";
										$str_dl .= "<dd>" . $str_body . "</dd>\n";
										
									}//endwhile
									
									$str_dl .= '</dl>';
									echo $str_dl;
						
								}//endif
								?>

								<!-- content end -->
								<?php echo makeRelational($rel_array) ?>
							</article>
						</main>
					</div><!-- mainWrap end -->
					<?php include( $DOC_ROOT . '/inc/side_inc.php');?>
				</div><!-- cont980 end -->
				<?php include( $DOC_ROOT . '/inc/footer_inc.php');?>
			</div><!-- contWrap end -->
		<?php include( $DOC_ROOT . '/inc/script_inc.php');?>
	</div><!-- allWrap end -->
</body>
</html>

<?php get_footer(); ?>