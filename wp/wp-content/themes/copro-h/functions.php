<?php
//自動挿入する meta 情報を削除する
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'start_post_rel_link');
remove_action('wp_head', 'parent_post_rel_link');
remove_action('wp_head', 'adjacent_posts_rel_link');


//RSS をサムネイル付き抜粋文にする
function diw_post_thumbnail_feeds($content){
	global $post;
	if(has_post_thumbnail($post->ID)){
		$content = '<div>' // ※
				   . get_the_post_thumbnail($post->ID,  'thumbnail') 
				   . '</div>' . $content; // ※
}
	return $content;
}

add_filter('the_excerpt_rss', 'diw_post_thumbnail_feeds');
add_filter('the_content_feed', 'diw_post_thumbnail_feeds');

// バージョン更新を非表示にする
add_filter('pre_site_transient_update_core', '__return_zero');
// APIによるバージョンチェックの通信をさせない
remove_action('wp_version_check', 'wp_version_check');
remove_action('admin_init', '_maybe_update_core');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'feed_links', 2);

// フッターWordPressリンクを非表示に
function custom_admin_footer() {
}
add_filter('admin_footer_text', 'custom_admin_footer');

//絵文字用スクリプトの停止
function disable_emoji() {
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );    
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );    
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
}
add_action( 'init', 'disable_emoji' );

//WPログイン画面カスタマイズ
function my_login_logo() {
?>
    <style type="text/css">
        body.login div#login h1 a {
            background-image: url('/assets/img/h1Logo.png');
            padding-bottom: 30px;
			width:160px;
			height:64px;
			background-size:160px;
        }
    </style>
<?php
}

//WP管理画面css
function my_admin_style() {
?>
	<style type="text/css">
		.customFormDl{
			box-sizing:border-box;
			width:100%;
			border:2px solid #f4f4f4;
			padding:5px;
			font-size:12px;
		}
		
		.customFormDl dt, .customFormDl dd{
			padding:5px;
			margin:0;
		}
		.customFormDl dt{
			font-weight:bold;
			border-bottom:2px solid #f4f4f4;
		}
		.customFormDl dd{
			padding-top:10px;
		}
		.customFormDl textarea, .customFormDl input[type="text"]{
			width:100%;
			margin-bottom:10px;
		}
	</style>
<?php
}
add_action('admin_print_styles', 'my_admin_style');


add_action( 'login_enqueue_scripts', 'my_login_logo' );
function login_logo_url() {
    return get_bloginfo('url');
}
add_filter('login_headerurl', 'login_logo_url');
function login_logo_title(){
    return get_bloginfo('name');
}
add_filter('login_headertitle','login_logo_title');

// 管理バーの項目を非表示
function remove_admin_bar_menu( $wp_admin_bar ) {
	$wp_admin_bar->remove_menu( 'wp-logo' ); // WordPressシンボルマーク
	$wp_admin_bar->remove_menu('my-account'); // マイアカウント
	
}
add_action( 'admin_bar_menu', 'remove_admin_bar_menu', 70 );

function mytheme_remove_item( $wp_admin_bar ) {
	$wp_admin_bar->remove_node('new-content'); // 新規投稿ボタン
	$wp_admin_bar->remove_node('comments'); // コメント
}
add_action( 'admin_bar_menu', 'mytheme_remove_item', 1000 );

// 管理バーのヘルプメニューを非表示にする
function my_admin_head(){
	echo '<style type="text/css">#contextual-help-link-wrap{display:none;}</style>';
}
add_action('admin_head', 'my_admin_head');

// 管理バーにログアウトを追加
function add_new_item_in_admin_bar() {
	global $wp_admin_bar;
	$wp_admin_bar->add_menu(array(
	'id' => 'new_item_in_admin_bar',
	'title' => __('ログアウト'),
	'href' => wp_logout_url()
	));
}
add_action('wp_before_admin_bar_render', 'add_new_item_in_admin_bar');


// メニューを非表示にする
function edit_admin_menus() {
	global $menu;
	remove_menu_page ( 'index.php' );// ダッシュボード
	remove_menu_page ( 'edit-comments.php' );// コメント
	remove_menu_page( 'all-in-one-seo-pack/aioseop_class.php' );// All in One SEO Pack
	remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=post_tag'); // 投稿 -> タグ
}
add_action( 'admin_menu', 'edit_admin_menus' );

function custom_admin_menu() {
	remove_menu_page( 'all-in-one-seo-pack/aioseop_class.php' );// All in One SEO Pack
}
add_action( 'admin_menu', 'custom_admin_menu', 1000 );

//ダッシュボードの項目を非表示にする
remove_action( 'welcome_panel', 'wp_welcome_panel' );
function example_remove_dashboard_widgets() {
	global $wp_meta_boxes;
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);   // 現在の状況（概要）
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);   // 最近のコメント
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);   // 被リンク
	unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);   // プラグイン
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);   // クイック投稿
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_recent_drafts']);   // 最近の下書き
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);   // WordPressブログ
	unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);   // WordPressフォーラム
}
add_action('wp_dashboard_setup', 'example_remove_dashboard_widgets');

//管理画面メニュー非表示処理
function remove_menus() {
    remove_menu_page('separator1');
	 if (!current_user_can('level_10')) { //level10以下のユーザーの場合メニューをunsetする
	remove_menu_page('index.php'); // ダッシュボード
	remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=post_tag'); // 投稿 -> タグ
	remove_menu_page('link-manager.php'); // リンク
	remove_menu_page('edit.php?post_type=page'); // 固定ページ
	remove_menu_page('edit-comments.php'); // コメント
	remove_menu_page('separator2'); // セパレータ1
	remove_menu_page('themes.php'); // 外観
	remove_menu_page('plugins.php'); // プラグイン
	remove_menu_page('users.php'); // ユーザー
	remove_menu_page('tools.php'); // ツール
	remove_menu_page('options-general.php'); // 設定
	remove_menu_page('profile.php'); // プロフィール(管理者以外のユーザー用)3
	}
}
add_action('admin_menu', 'remove_menus');

//投稿一覧項目非表示
function custom_columns ($columns) {
    unset($columns['tags']);
    unset($columns['comments']);
    return $columns;
}
add_filter( 'manage_posts_columns', 'custom_columns' );

//メニュー名変更
function change_post_menu_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'お知らせ';
    $submenu['edit.php'][5][0] = 'お知らせ一覧';
    $submenu['edit.php'][10][0] = '新しく【お知らせ】記事を書く';
    $submenu['edit.php'][15][0] = 'お知らせのカテゴリ';
    $submenu['edit.php'][16][0] = 'タグ';
    //echo ";
}
function change_post_object_label() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'お知らせ';
    $labels->singular_name = 'お知らせ';
    $labels->add_new = _x('新しく【お知らせ】記事を書く', 'お知らせ');
    $labels->add_new_item = 'お知らせ記事を書く';
    $labels->edit_item = 'お知らせの編集';
    $labels->new_item = '新規お知らせ';
    $labels->view_item = 'ページを表示';
    $labels->search_items = 'お知らせの検索';
    $labels->not_found = '記事が見つかりませんでした';
    $labels->not_found_in_trash = 'ゴミ箱に記事は見つかりませんでした';
}
add_action( 'init', 'change_post_object_label' );
add_action( 'admin_menu', 'change_post_menu_label' );

//パスワード保護の記事を一覧から除外
add_filter('posts_where', 'my_posts_where');
function my_posts_where($where){
	global $wpdb;
	if(!is_singular() && !is_admin()){ 
		$where .= " AND $wpdb->posts.post_password = ''";
	}
	return $where;
}

//年別一覧表示
function get_archives_by_fiscal_year( $args = '' ) {
	global $wpdb, $wp_locale;
	$defaults = array (
		'post_type' => 'post',
		'limit' => '',
		'format' => 'html',
		'before' => '',
		'after' => '',
		'show_post_count' => false,
		'echo' => 1
	);
	$r = wp_parse_args( $args, $defaults );
	extract ( $r, EXTR_SKIP );
	if ( '' != $limit ) {
		$limit = absint( $limit );
		$limit = ' LIMIT ' . $limit;
	}
	$arcresults = (array) $wpdb->get_results(
		"SELECT YEAR(ADDDATE(post_date, INTERVAL -3 MONTH)) AS `year`, COUNT(ID) AS `posts`
		FROM $wpdb->posts
		WHERE post_type = '$post_type' AND post_status = 'publish'
		GROUP BY YEAR(ADDDATE(post_date, INTERVAL -3 MONTH))
		ORDER BY post_date DESC
		$limit"
	);
	return $arcresults;
}

//投稿ページへ表示するカスタムボックスを定義する
add_action('admin_menu', 'add_custom_inputbox');
//入力したデータの更新処理
add_action('save_post', 'save_custom_postdata');
 
//#################################
//投稿ページ用
//#################################
//投稿ページに表示される"追加情報"の設定
function add_custom_inputbox() {
	add_meta_box( 'coproid','追加情報', 'copro_custom_field', 'post', 'normal' );
}
 
//投稿ページに表示されるカスタムフィールド
function copro_custom_field(){
	$id = get_the_ID();
	//カスタムフィールドの値を取得
	$list_text = get_post_meta($id,'list_text',true);
	$detail_check = get_post_meta($id,'detail_check',true);
	if( $detail_check == 1 ){
		$checked = ' checked';
	}else{
		$checked = '';
	}
	$detail_url = get_post_meta($id,'detail_url',true);
	
	$str = '<dl class="customFormDl"><dt>一覧で表示される内容を変更</dt><dd><textarea name="list_text" rows="6">' . $list_text . '</textarea><br />入力すると一覧で記事タイトルの代わりに表示されます。</dd></dl>';
	$str .= '<dl class="customFormDl"><dt>詳しくはこちら</dt><dd><label><input type="checkbox" value="1" name="detail_check"' . $checked . '>チェックすると「詳しくはこちら」で個別ページへリンクします。</label></dd></dl>';
	$str .= '<dl class="customFormDl"><dt>詳しくはこちらのリンクURL</dt><dd><input type="text" name="detail_url" value="' . $detail_url . '"><br />「詳しくはこちら」のリンクURLを個別ページ以外で指定できます（空欄の場合は個別ページへリンク）。</dd></dl>';
	echo $str;
	//<input type="text" name="coprotext" value="' . $coprotext . '"><br />';
}
//#################################
//更新処理
//#################################
/*投稿ボタンを押した際のデータ更新と保存*/
function save_custom_postdata($post_id){
	//入力した値(postされた値)
	$list_text=isset($_POST['list_text']) ? $_POST['list_text']: null;
	$detail_check=isset($_POST['detail_check']) ? $_POST['detail_check']: null;
	$detail_url=isset($_POST['detail_url']) ? $_POST['detail_url']: null;
	//DBに登録してあるデータ
	$list_text_ex = get_post_meta($post_id, 'list_text', true);
	if($list_text){
		update_post_meta($post_id, 'list_text',$list_text);
	}else{
		delete_post_meta($post_id, 'list_text',$list_text_ex);
	}
	$detail_check_ex = get_post_meta($post_id, 'detail_check', true);
	if($detail_check){
		update_post_meta($post_id, 'detail_check',$detail_check);
	}else{
		delete_post_meta($post_id, 'detail_check',$detail_check_ex);
	}
	$detail_url_ex = get_post_meta($post_id, 'detail_url', true);
	if($detail_url){
		update_post_meta($post_id, 'detail_url',$detail_url);
	}else{
		delete_post_meta($post_id, 'detail_url',$detail_url_ex);
	}
}