<?php
//必須ファイル読み込み
require_once('../system/function.php');

get_header();

//ページ設定
$str_dsc = get_the_title();
$str_tit = 'お知らせ';
$str_kwd = get_the_title();
$str_page_id = 'pageNews';

?>
<!DOCTYPE html>
<html lang="ja">
<head>
	<?php include( $DOC_ROOT . '/inc/meta_inc.php');?>
	<link rel="stylesheet" type="text/css" href="/assets/css/news.css">
</head>
<body id="<?php echo $str_page_id; ?>">
	<div id="allWrap">
		<?php include( $DOC_ROOT . '/inc/header_inc.php');?>
			<div class="contWrap">
				<div class="cont980 ibWrap">
					<nav class="breadCrumb">
						<ul>
							<li><a href="/">ホーム</a></li>
							<li><i class="fa fa-chevron-right"></i><a href="/news/">お知らせ</a></li>
							<li><i class="fa fa-chevron-right"></i><?php echo get_the_title(); ?></li>
						</ul>
					</nav>
					<h2 class="h2_line h2_short"><?php echo $str_tit; ?></h2>
					<div id="mainWrap" class="contMain">
						<main>
							<article>
								<!-- content-->
								<div class="div_newsDet">
									<?php
										while ( have_posts() ){
											the_post();
											$cat = get_the_category();
											$cat_name = '';
											if(is_array($cat)){
												foreach( $cat as $cate ) {
													$cat_name .= '<a href="/category/' . $cate->category_nicename . '">' . $cate->cat_name . '</a>';
												}
											}
											$str .= '<h3 class="h3_tit">' . get_the_title() . '</h3>';
											$str .= '<div class="wpContDate"><span>' . get_the_time('Y.m.d') .'</span>'. $cat_name . '</div>';
											$str .= '<div class="wpCont">';
											$str .= get_the_content();
											$str .= '</div>';
										}
										echo $str;
									?>
								</div>
								<!-- content end -->
								<?php echo makeRelational($rel_array) ?>
							</article>
						</main>
					</div><!-- mainWrap end -->
					<?php include( $DOC_ROOT . '/inc/side_inc.php');?>
				</div><!-- cont980 end -->
				<?php include( $DOC_ROOT . '/inc/footer_inc.php');?>
			</div><!-- contWrap end -->
		<?php include( $DOC_ROOT . '/inc/script_inc.php');?>
	</div><!-- allWrap end -->
</body>
</html>

<?php get_footer(); ?>
