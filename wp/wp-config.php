<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link http://wpdocs.sourceforge.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86
 *
 * @package WordPress
 */

// 注意: 
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.sourceforge.jp/Codex:%E8%AB%87%E8%A9%B1%E5%AE%A4 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define('DB_NAME', 'ae134wc6nt_wordpress');

/** MySQL データベースのユーザー名 */
define('DB_USER', 'ae134wc6nt');

/** MySQL データベースのパスワード */
define('DB_PASSWORD', 'FFfVWBFt');

/** MySQL のホスト名 */
define('DB_HOST', '127.0.0.1:3307');

/** データベースのテーブルを作成する際のデータベースの文字セット */
define('DB_CHARSET', 'utf8mb4');

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ',~>@#SX759e%:PD;6&g%/fj`1#Qmnyxg|;X+QmfUk1]0fR=Y<Wj!9,bj1<zW<A(-');
define('SECURE_AUTH_KEY',  'bL8b5Y}(t>SC}riot8H>|s<Z!V[&#{yZ&k-h-4O/hm$-^/!QV-}+:_5<!/Tm;+]/');
define('LOGGED_IN_KEY',    'QTkD)h4!q8o[-hVWDk-!Q^JStPxr}j~-;XyPa1.0i34C`f&7AKl5+@gf]f:]H)te');
define('NONCE_KEY',        'ngHmf%m8qVE%T5KZZ`dHJ8o^nl<>.R!^%KGkUFr$ttIW((|wP:Q4y(eCsm4BjN$=');
define('AUTH_SALT',        'tS5p=P]>@s/-=/8aU<h<u#Z;+(<f/$KdBb4hi.Nb|Jc+43^}Rc5 `Y1R*{J9tM 9');
define('SECURE_AUTH_SALT', '%ZkN3 tKuD}L<X;wQn|(<{GXc}erjZz$#)$<8kGWMYrKBXJo[G{j7V]Y*b3||.oi');
define('LOGGED_IN_SALT',   '>uJv.LGqafS-}E^+i5|a^3?=-O,HF!H82BE_bbU|pbF}fg-iJH->Pn|*3;xbuecV');
define('NONCE_SALT',       'xO`IV1<St1UmRintej#9rU7OXfdP#gHe3$4A|}`S#ToKj;HQ9{I$-5JBF{reb1Pk');

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix  = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数については Codex をご覧ください。
 *
 * @link http://wpdocs.osdn.jp/WordPress%E3%81%A7%E3%81%AE%E3%83%87%E3%83%90%E3%83%83%E3%82%B0
 */
define('WP_DEBUG', false);

/* 編集が必要なのはここまでです ! WordPress でブログをお楽しみください。 */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
